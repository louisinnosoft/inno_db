/*
目的:找出今年的預算表,產生全部的次年預算表
做法:
1.只要專案上面立案單的狀態沒推到”財務結案”，都要產生一個次年專案預算表
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;
string strNG="";
int intOk=0;	
int intAll=0;
try
{
	aml = "<AML>";
	aml += "<Item type='In_Budget' action='get'>";
	aml += "<state condition='ne'>Closed</state>";
	aml += "<in_is_template>0</in_is_template>";
	aml += "<in_year>" + System.DateTime.Now.ToString("yyyy") + "</in_year>";
	//aml += "<in_year>2017</in_year>";
	aml += "</Item></AML>";
	Item itmBudgets = inn.applyAML(aml);
	intAll = itmBudgets.getItemCount();
	for(int i=0;i<itmBudgets.getItemCount();i++)
	{
		Item itmBudget = itmBudgets.getItemByIndex(i);
		try{
			itmBudget = itmBudget.apply("In_CreateNextBudget");	
			intOk +=1;
		}
		catch(Exception ex1)
		{
			strNG += itmBudget.getProperty("item_number","") + ":" + ex1.Message + "\n";
		}
	}
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
	strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
	strMethodName = strMethodName.Replace("EventArgs)","");
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
if(strNG!="")
{
	Innosoft.InnUtility.AddLog(strNG,"建立次年預算表錯誤-" + System.DateTime.Now.AddYears(1).ToString("yyyy"));
	_InnH.SendEmailToAdmin("建立次年預算表錯誤-" + System.DateTime.Now.AddYears(1).ToString("yyyy"),strNG.Replace("\n","<br>"));
}
string strOk = "";
strOk = "總共:" +  intAll.ToString() + "筆\n";
strOk += "成功:" + intOk.ToString() + "筆\n";
strOk += "失敗說明:" + strNG;
Innosoft.InnUtility.AddLog(strOk,"建立次年預算表-" + System.DateTime.Now.AddYears(1).ToString("yyyy"));

return inn.newResult(strOk);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CreateNextBudget_All' and [Method].is_current='1'">
<config_id>61D8D50F5B14474EA9496FAD74D0E392</config_id>
<name>In_CreateNextBudget_All</name>
<comments>出今年的預算表,產生全部的次年預算表</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
