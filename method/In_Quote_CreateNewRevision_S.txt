// PE_CreateNewRevision
// Called from a server action. Creates a new revision outside of change control
// //System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string sql = "";
string thisType = this.getType();
string thisName = this.getProperty("keyed_name","");

if(this.isNew() || this.getAttribute("isTemp", "").Equals("1", StringComparison.OrdinalIgnoreCase))
{
  return inn.newError(CCO.ErrorLookup.Lookup("PE_CreateNewRevision_MustBeSaved", thisType));
}

bool isInReleasedState = String.Equals(this.getProperty("state",""), "Done", StringComparison.Ordinal);

// Item must be in the Released state
if (!isInReleasedState)
{
	
  return inn.newError(_InnH.Translate("本報價單狀態為[" + this.getProperty("state","") + "]無法改版"));
}

// Make sure the current user is in the Owner identity

string strSalesUserId = this.getProperty("in_sales","");
if(strSalesUserId=="")
	
	return inn.newError(_InnH.Translate("您無權限改版本報價單"));
string ownedById = _InnH.GetIdentityByUserId(strSalesUserId).getID();
if (!CCO.Permissions.IdentityListHasId(Aras.Server.Security.Permissions.Current.IdentitiesList, ownedById))
{
  return inn.newError(CCO.ErrorLookup.Lookup("PE_ManualRelease_YouMustBeAMember", this.getPropertyAttribute("owned_by_id","keyed_name","Owner")));
}

// Make sure the item is not used on any change items
Item checkResult = this.apply("PE_CheckChangeItemUsage");
if(checkResult.isError())
{
    return checkResult;
}

// Grant 'Aras PLM' permissions
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
    // Version and unlock the item
    Item resItem = this.apply("version");
    
    if(resItem.isError())
    {
        return resItem;
    }
    else
    {
        if(this.getType() == "In_Quote")
        {
            if(this.getProperty("in_wfp_state","") == "Closed")
            {
                sql = "Update [" + this.getType().Replace(" ","_") + "] set in_wff_id=null,in_wfp_id=null,in_wfp_state=null,in_current_activity=null where id='" + resItem.getID() + "'";
	inn.applySQL(sql);
            }
        }
    }
    resItem = resItem.apply("unlock"); 
    return resItem;
}
finally
{
  // Revoke 'Aras PLM' permissions
  if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Quote_CreateNewRevision_S' and [Method].is_current='1'">
<config_id>6EF0318EA4BA4CFBB05EE635D0DDEBC3</config_id>
<name>In_Quote_CreateNewRevision_S</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
