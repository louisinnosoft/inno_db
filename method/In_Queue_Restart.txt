//目的:重啟排成佇列
//做法:將In_Queue的In_State欄位強制更新為start

Innovator inn = this.getInnovator();
this.setProperty("in_state","start");
this.apply("edit");

return this;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Queue_Restart' and [Method].is_current='1'">
<config_id>DB4A489E2D084F97A9D650CB44CA5785</config_id>
<name>In_Queue_Restart</name>
<comments>Queue</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
