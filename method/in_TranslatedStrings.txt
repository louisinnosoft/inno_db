//假設語言順序是-繁體、簡體、英文

//string Lans = "zh-tw,zh-cn,en-us";
string Lans = "zt,zc,en";
string[] AllLan = Lans.Split(',');
string Text = this.getProperty("AllText", "");
string Format = this.getProperty("Format", ",");
string[] AllText;
string UserLan = "";
Item ResultItem = this.newItem();

if(Text == "") return null;

AllText = Text.Split(Format[0]);

//UserLan = System.Web.HttpContext.Current.Request.UserLanguages[0]; //回傳zh-tw及zh-cn等
//UserLan = this.getInnovator.getI18NSessionContext().GetLanguageSuffix(); //回傳為_zt及_zc
UserLan = this.getInnovator().getI18NSessionContext().GetLanguageCode(); //回傳為zt及zc
UserLan = UserLan.ToLower();

for(int i = 0; i < AllLan.Length; i++)
{
	if(UserLan == AllLan[i])
	{
		ResultItem.setProperty("AllText", AllText[i]);
		return ResultItem;
	}
}

ResultItem.setProperty("AllText", AllText[0]);
return ResultItem;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='in_TranslatedStrings' and [Method].is_current='1'">
<config_id>D28A0A88956343319DF1B99020B78393</config_id>
<name>in_TranslatedStrings</name>
<comments>Add by IN.</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
