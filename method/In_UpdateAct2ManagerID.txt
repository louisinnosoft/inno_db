//更新專案計畫的角色
// //System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

string sql = "";

try
{
    sql = "SELECT[related_id],[role]FROM[Project_Team]WHERE[source_id]='"+this.getID()+"'";
    Item itmSQL = inn.applySQL(sql);

    for(int i=0;i<itmSQL.getItemCount();i++)
    {
        sql = "UPDATE[Activity2]SET[managed_by_id]='"+itmSQL.getItemByIndex(i).getProperty("related_id")+"' WHERE[lead_role]='"+itmSQL.getItemByIndex(i).getProperty("role")+"'AND[in_proj_id]='"+this.getID()+"'";
        inn.applySQL(sql);
    }

    sql = "SELECT[related_id],[role]FROM[Project_Team]WHERE[source_id]='"+this.getID()+"'";
    itmSQL = inn.applySQL(sql);
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateAct2ManagerID' and [Method].is_current='1'">
<config_id>9723BCB75A974945868BF76512C7B5B6</config_id>
<name>In_UpdateAct2ManagerID</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
