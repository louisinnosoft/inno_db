if (this.getProperty('crawler_state', '') === 'Active') {

	var crawlerState = this.newItem('ES_CrawlerState', 'get');
	crawlerState.setAttribute('select', 'current_action, next_action');
	crawlerState.setProperty('source_id', this.getID());
	crawlerState = crawlerState.apply();

	if (!crawlerState.isError() && crawlerState.getItemCount() === 1) {

		if (crawlerState.getProperty('current_action', '') !== 'Stop current iteration' && crawlerState.getProperty('current_action', '') !== 'Delete') {
			crawlerState.setProperty('next_action', 'Stop current iteration');

			var iLockStatus = crawlerState.getLockStatus();

			if (iLockStatus === 0) {
				crawlerState.apply('edit');
			}

			if (iLockStatus === 1) {
				crawlerState.apply('update');
			}
		} else {
			aras.AlertError('The crawler isn\'t running.');
		}
	} else {
		aras.AlertError(crawlerState.getErrorMessage());
	}
} else {
	aras.AlertError('A crawler should be in \'Active\' state to perform any operations.');
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_StopCrawler' and [Method].is_current='1'">
<config_id>9A41F1F6643042488F1B0A96D7170171</config_id>
<name>ES_StopCrawler</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
