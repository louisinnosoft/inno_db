/*
 * 更新 Project 的 Team 物件
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
	Innosoft.InnovatorHelper _InnS = new Innosoft.InnovatorHelper(inn);
	//UpdateProjectTeam(Item Context,bool IsOverWrite)
	Item itmResult = _InnS.UpdateProjectTeam(this,false);
	
	if (itmResult.isError()) return itmResult;
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateProjectTeam' and [Method].is_current='1'">
<config_id>CA623EE7FF964565AADF9BB7299DE5CE</config_id>
<name>In_UpdateProjectTeam</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
