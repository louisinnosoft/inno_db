Innovator inn = this.getInnovator();
try{
    Item itmR = inn.applyMethod("test_NewError","");
    if(itmR.isError())
    {
        //System.Diagnostics.Debugger.Break();
    }
}
catch(Aras.Server.Core.InnovatorServerException ex)
{
    System.Runtime.Serialization.SerializationInfo info=null;
    System.Runtime.Serialization.StreamingContext context=new System.Runtime.Serialization.StreamingContext();
    ex.GetObjectData( info,context );
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='test_caller2' and [Method].is_current='1'">
<config_id>48034DBD2038444FB4EB09758A64D227</config_id>
<name>test_caller2</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
