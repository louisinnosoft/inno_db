/*
    更新使用原創特製改版功能的物件。基本由InnoJSON-ApplyAML或內部呼叫。
    先apply原始的AML並解鎖(若物件action不是edit)，然後用取得的物件去aqpplyMethod("In_updateVersion")
        注意：請務必在apply物件前將物件的action設為update或edit以避免重複apply同一個方法造成無限迴圈。
    
*/

System.Diagnostics.Debugger.Break();
Innovator inn=this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

try{
    this.setAttribute("action","update");
    Item itmQ=this.apply();
    itmQ.unlockItem();
    if(itmQ.isError() || itmQ.isEmpty()){
        throw new Exception(itmQ.dom.InnerXml);
    }
    
    itmQ=itmQ.apply("In_UpdateVersion");
    
    if(itmQ.isError()|| itmQ.isEmpty()){
        throw new Exception(itmQ.dom.InnerXml);
    }else{
        return inn.newResult(true.ToString());
        
    }
    
    
    
}catch(Exception exx){
        string strError = exx.Message + "\n";
	string strErrorDetail="";
	string strParameter="\n itemtype="+this.getType()+" , itemid= "+this.getID();
	_InnH.AddLog(strErrorDetail+strParameter,"Error");
	strError = strError.Replace("<br/>","");

	throw new Exception(_InnH.Translate(strError+strParameter));

    
}


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateCustomVersionable' and [Method].is_current='1'">
<config_id>3337AAE9A46C4033A1CB4D05FDE2C9E4</config_id>
<name>In_UpdateCustomVersionable</name>
<comments>更新使用原創客製改版功能的物件(目前Document &amp; in_proposal)。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
