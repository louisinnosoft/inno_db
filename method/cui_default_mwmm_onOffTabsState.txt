var topWindow = aras.getMostTopWindowWithAras(window);
var menuFrame = topWindow.menu;
if (menuFrame && menuFrame.onOffTabsStateCommand) {
	aras.setPreferenceItemProperties('Core_GlobalLayout', null, {'core_tabs_state': 'tabs off'});
	menuFrame.onOffTabsStateCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_onOffTabsState' and [Method].is_current='1'">
<config_id>3A8A41C4A4C34AE480202FFDFA7642FD</config_id>
<name>cui_default_mwmm_onOffTabsState</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
