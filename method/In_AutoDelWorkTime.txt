/*
目的:
做法:
Method(Run)
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string aml = "";

//取得所有的使用者(無法登入)
aml = "<AML>";
aml += "<Item type='User' action='get'>";
aml += "<logon_enabled>0</logon_enabled>";
aml += "</Item></AML>";
Item itmUsers = inn.applyAML(aml);
for(int i = 0;i < itmUsers.getItemCount();i++){
    Item itmUser = itmUsers.getItemByIndex(i);
    
    aml = "<AML>";
    aml += "<Item type='Identity' action='get'>";
    aml += "<in_user>" + itmUser.getID() + "</in_user>";
    aml += "</Item></AML>";
    Item itmIdentity = inn.applyAML(aml);
    
    aml = "<AML>";
    aml += "<Item type='In_PerlWorkRecord' action='delete' where = \"[owned_by_id] = '" + itmIdentity.getID() + "'\">";
    aml += "<owned_by_id>" + itmIdentity.getID() + "</owned_by_id>";
    aml += "</Item></AML>";
    Item itmPerlWorkRecord = inn.applyAML(aml);
    
    aml = "<AML>";
    aml += "<Item type='In_TimeRecord' action='delete' where = \"[owned_by_id] = '" + itmIdentity.getID() + "'\">";
    aml += "<owned_by_id>" + itmIdentity.getID() + "</owned_by_id>";
    aml += "</Item></AML>";
    Item itmTimeRecord = inn.applyAML(aml);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AutoDelWorkTime' and [Method].is_current='1'">
<config_id>90438024A947402A8DB2078169B30596</config_id>
<name>In_AutoDelWorkTime</name>
<comments>自動刪除工時紀錄表</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
