return ModulesManager.using(
	['aras.innovator.CMF/CMFItemWindowView', 'aras.innovator.core.ItemWindow/DefaultItemWindowCreator']).then(function(View, Creator) {
		var view = new View(inDom,inArgs);
		var creator = new Creator(view);
		return creator.ShowView();
	});

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_OnShowContainer' and [Method].is_current='1'">
<config_id>55D817546D784B62AD2282E455352072</config_id>
<name>cmf_OnShowContainer</name>
<comments>Loads enhanced view for CMF document</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
