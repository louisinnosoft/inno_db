/*
目的:挑選本合約對應的預算中的非人事預算的會科
做法:
1.依據表頭專案欄位找到目前啟用中的預算表
2.找出啟用預算表的非人事預算會科id
位置:
合約的in_partitem屬性 onSearchDialog
*/

//1.依據表頭專案欄位找到目前啟用中的預算表
var inn = this.getInnovator();
var aml = "";
var strIDList = "";
var arrPartItemID = new Array();
var retObj = new Object();

if(parent.thisItem.getProperty("in_project","") === ""){
	alert(inn.applyMethod("In_Translate","<text>請先挑選專案</text>").getResult());

	retObj["in_company"] = {filterValue:0,isFilterFixed:true}; //故意讓in_company為錯誤值,查不到資料
	return retObj;
}else{
    var itmBudgetId = inn.applyMethod("In_GetCurrentBudgetByProjectId","<ProjectId>" + parent.thisItem.getProperty("in_project","") + "</ProjectId>");
    if(itmBudgetId.isError()){
		alert(itmBudgetId.getErrorString());

		retObj["in_company"] = {filterValue:0,isFilterFixed:true}; //故意讓in_company為錯誤值,查不到資料
		return retObj;
	}
	var strBudgetId = itmBudgetId.getResult();

	//2.找出啟用預算表的非人事預算會科id
	aml = "<AML>";
	aml += "<Item type='In_Budget_RD_Budget' action='get'>";
	aml += "<related_id>";
	aml += "<Item type='Part' action='get'>";
	aml += "<in_accounting_subjects>Outsource costs</in_accounting_subjects>";
	aml += "</Item>";
	aml += "</related_id>";
	aml += "<source_id>" + strBudgetId + "</source_id>";
	aml += "</Item></AML>";
	var itmRD_Budgets = inn.applyAML(aml);

	for (var i=0; i < itmRD_Budgets.getItemCount(); i++) {
	    var itmRD_Budget = itmRD_Budgets.getItemByIndex(i);
	    arrPartItemID[i]=itmRD_Budget.getProperty("related_id","");
	}

	if(itmRD_Budgets.getItemCount()==0)
    {
        alert(inn.applyMethod("In_Translate","<text>本專案預算尚未編列「非人事費預算」,因此無法挑選預算項目</text>").getResult());

		retObj["in_company"] = {filterValue:0,isFilterFixed:true}; //故意讓in_company為錯誤值,查不到資料
		return retObj;
    }
	else
	{
	    strIDList = arrPartItemID.join(",");
	    inArgs.QryItem.item.setAttribute("idlist",strIDList);
	    //return inArgs;
	    return("");
	}
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='in_cfg_PickBudgetPartItems' and [Method].is_current='1'">
<config_id>9D2D84B7A0D74C1C90A5DCAEC2C0C7FE</config_id>
<name>in_cfg_PickBudgetPartItems</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
