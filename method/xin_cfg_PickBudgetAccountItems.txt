/*
目的:挑選本合約對應的預算中的非人事預算的會科
做法:
1.依據表頭專案欄位找到目前啟用中的預算表
2.找出啟用預算表的非人事預算會科id
位置:
合約的in_accounting屬性 onSearchDialog
*/

//1.依據表頭專案欄位找到目前啟用中的預算表
var strProjectId = parent.thisItem.getProperty("in_project","");
//var idlist="DC8426AEBDDD4EE7BEE1444E22F198F3"; //隨便給一個id
var idlist ="";
var strPartIDList = "";
var retObj = new Object();

if(strProjectId==""){
	alert("請先挑選專案");
	//inArgs.QryItem.item.setAttribute("idlist",idlist);
	//return inArgs;
	retObj["in_company"] = {filterValue:'NA',isFilterFixed:true}; //故意讓in_company為錯誤值,查不到資料
	return retObj;
}else{
	var inn = this.getInnovator();
	var itmBudgetId = inn.applyMethod("In_GetCurrentBudgetByProjectId","<ProjectId>" + strProjectId + "</ProjectId>");
	if(itmBudgetId.isError()){
		alert(itmBudgetId.getErrorString());
		
		retObj["in_company"] = {filterValue:'NA',isFilterFixed:true}; //故意讓in_company為錯誤值,查不到資料
		return retObj;
	}
	var strBudgetId = itmBudgetId.getResult();
	//alert(strBudgetId);
	//2.找出啟用預算表的非人事預算會科id
	var aml = "<AML>";
	aml += "<Item type='In_Budget_RD_Budget' action='get'>";
	
	aml += "<related_id>";
	aml += "<Item type='Part' action='get'>";
	aml += "<in_part_type>3</in_part_type>";
	aml += "</Item>";
	aml += "</related_id>";
	
	aml += "<source_id>" + strBudgetId + "</source_id>";
	aml += "</Item></AML>";
	var itmRD_Budgets = inn.applyAML(aml);
	
	var arrAccountItemIds = new Array();
	var arrPartItemIds = new Array();
	for (var i=0; i < itmRD_Budgets.getItemCount(); i++) {
		var itmRD_Budget = itmRD_Budgets.getItemByIndex(i);
		arrAccountItemIds[i]=itmRD_Budget.getProperty("in_accounting","");
		arrPartItemIds[i]=itmRD_Budget.getProperty("related_id","");
	}
	
	if(itmRD_Budgets.getItemCount()==0)
    {
        alert("本專案預算尚未編列「非人事費預算」,因此無法挑選預算項目");
		retObj["in_company"] = {filterValue:'NA',isFilterFixed:true}; //故意讓in_company為錯誤值,查不到資料
		return retObj;
    }
	else
	{
	    idlist=arrAccountItemIds.join(",");
	    strPartIDList=arrPartItemIds.join(",");
    	//alert(idlist);
    	parent.thisItem.setProperty("in_part_id_list",strPartIDList);
	    inArgs.QryItem.item.setAttribute("idlist",idlist);
	}
	//retObj["in_company"] = {filterValue:thisItem.getProperty("in_company",""),isFilterFixed:true};
	//return retObj;
	return inArgs;
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='xin_cfg_PickBudgetAccountItems' and [Method].is_current='1'">
<config_id>BCE4B27522104715BA6BFA48C1BB1CAE</config_id>
<name>xin_cfg_PickBudgetAccountItems</name>
<comments>選本合約對應的預算中的非人事預算的會科</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
