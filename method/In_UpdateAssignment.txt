/*
Method Name: In_UpdateAssignment
Server-side C#
郵件訊息Activity的action改為此method
*/
//Created by Tom  2017/05/16
Item activity=this.getInnovator().getItemById("Activity",this.getID());
activity.apply("In_AutoAssignment");
return activity;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateAssignment' and [Method].is_current='1'">
<config_id>F9D4D720CF9349F8B4412F2EA1EDC20B</config_id>
<name>In_UpdateAssignment</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
