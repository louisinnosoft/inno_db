if (inArgs.isReinit) {
	var topWindow = aras.getMostTopWindowWithAras(window);
	var workerFrame = topWindow.work;
	if (workerFrame.itemTypeName === 'InBasket Task') {
		return {'cui_disabled': false};
	}
	if (workerFrame && workerFrame.grid) {
		var itemIDs = workerFrame.grid.getSelectedItemIds();
		var state = itemIDs && itemIDs.length === 1;
		return {'cui_disabled': !state};
	}
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_mwt_print' and [Method].is_current='1'">
<config_id>71A7F823D7404A7BA06C079A519AC9A7</config_id>
<name>cui_reinit_mwt_print</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
