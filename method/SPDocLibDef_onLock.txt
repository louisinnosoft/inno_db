var refreshButton = document.forms.MainDataForm.elements['refresh_button'];
refreshButton.disabled = !document.isEditMode;

var authNode = document.item.selectSingleNode('authentication_type');
var userField = document.forms.MainDataForm.elements['sp_user'];
var passField = document.forms.MainDataForm.elements['sp_password'];
var domainField = document.forms.MainDataForm.elements['sp_domain'];

if (document.isEditMode) {
	if (authNode.text === 'WindowsAuthenticationPreferred' ||
		authNode.text === 'DedicatedUserOnly') {
		userField.disabled = false;
		passField.disabled = false;
		domainField.disabled = false;
	} else {
		userField.disabled = true;
		passField.disabled = true;
		domainField.disabled = true;
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SPDocLibDef_onLock' and [Method].is_current='1'">
<config_id>18E150515921466BB4108B0C6AB75EA8</config_id>
<name>SPDocLibDef_onLock</name>
<comments>Handle changes in UI of SPDocLib form depends on authentication type</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
