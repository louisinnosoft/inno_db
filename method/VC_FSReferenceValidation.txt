var reference = this.getProperty("reference");
if (string.Compare(reference, "this", true) == 0)
{
	var message = string.Format(CCO.ErrorLookup.Lookup("SSVC_ReferencePropertyToThis"), this.getAttribute("type"));
	throw new ArgumentException(message);
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_FSReferenceValidation' and [Method].is_current='1'">
<config_id>53A6469C9F9345679C8E6C9683ACAC35</config_id>
<name>VC_FSReferenceValidation</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
