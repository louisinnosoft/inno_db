var errorMsg = "";
	
if (top.aras.isTempEx(this.node) || top.aras.isDirtyEx(this.node) || top.aras.isNew(this.node)) {
	errorMsg = top.aras.getResource("Project", "project.cannot_be_cloned", "Project Template");
	top.aras.AlertError(errorMsg);
	return;
}

var clone = this.newItem();
clone.loadAML("<AML>" + this.node.xml + "</AML>");
clone.setAttribute("projectCloneMode", "CreateTemplateFromTemplate");
var newTemplate = clone.apply("Project_CloneProjectOrTemplate");
if (newTemplate.isError()) {
	 top.aras.AlertError(newTemplate.getErrorString(), newTemplate.getErrorDetail());
}
top.aras.uiShowItemEx(newTemplate.node);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Project_CreateTemplateFromTempl' and [Method].is_current='1'">
<config_id>58CFC2DE8AC34B26A1AD3E3E55870528</config_id>
<name>Project_CreateTemplateFromTempl</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
