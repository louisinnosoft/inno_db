var topWindow = aras.getMostTopWindowWithAras(window);
var menuFrame = topWindow.menu;
if (menuFrame && menuFrame.onShowMainTreeCommand) {
	var menuItem = inArgs;
	var currChk = menuItem.getCheck();
	aras.setVariable('ShowMainTree', currChk);
	menuFrame.onShowMainTreeCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_onShowMainTree' and [Method].is_current='1'">
<config_id>A3DDDB34D9F04EB3948DABFB4FB6553C</config_id>
<name>cui_default_mwmm_onShowMainTree</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
