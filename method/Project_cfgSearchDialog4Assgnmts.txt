var projTemplateNd = (top.item && top.item.xml) ? top.item.selectSingleNode("ancestor-or-self::Item[@type='Project Template']") : null;
if (projTemplateNd)
  top.aras.AlertError(top.aras.getResource("project", "pr_methods.create_assignments_using_no_related"));
var retObj = new Object();
retObj["is_alias"] = { filterValue:"1", isFilterFixed:true };
return retObj;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Project_cfgSearchDialog4Assgnmts' and [Method].is_current='1'">
<config_id>69A37C52C73648B4ADAB7BF172F96355</config_id>
<name>Project_cfgSearchDialog4Assgnmts</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
