if (inArgs.isReinit) {
	var topWindow = aras.getMostTopWindowWithAras(window);
	var workerFrame = topWindow.work;
	if (workerFrame.itemTypeName === 'InBasket Task') {
		return {'cui_disabled': true};
	}
	if (Object.getOwnPropertyNames(inArgs.eventState).length === 0) {
		var states = aras.evalMethod('cui_reinit_calculate_states', '', {eventType: inArgs.eventType});
		var keys = Object.keys(states);
		for (var i = 0; i < keys.length; i++) {
			inArgs.eventState[keys[i]] = states[keys[i]];
		}
	}
	if (workerFrame) {
		return {'cui_disabled': !(inArgs.eventState.isPurge && !isFunctionDisabled(workerFrame.itemTypeName, 'Delete'))};
	}
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_mwt_delete' and [Method].is_current='1'">
<config_id>EE5CA17530F149FC8484AEF8D88123E8</config_id>
<name>cui_reinit_mwt_delete</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
