/*
    本方法由物件主體以item.apply("In_ApplyAfterAppGet")呼叫。
    本方法假設使用之物件為查詢結果，已帶有tupeId，且為單一物件。若無typeID,則不做任何動作。
    呼叫物件的"onAfterAppGet"事件。
    
*/
Innovator inn=this.getInnovator();
string strItemTypeId=this.getAttribute("typeId","no_data");
if(strItemTypeId=="no_data"){
    return this;
}
Item itmR=this;
Item itmQ=inn.newItem("Server Event","get");
itmQ.setProperty("source_id",strItemTypeId);
itmQ.setProperty("server_event","onAfterAppGet");
itmQ.setAttribute("select","related_id");
itmQ=itmQ.apply();


int intSeCount=itmQ.getItemCount();
for(int y=0;y<intSeCount;y++){
    Item relatedMethod=itmQ.getItemByIndex(y);
    string strMethodName=relatedMethod.getRelatedItem().getProperty("name","no_data");
    if(strMethodName=="no_data"){continue;}
    itmR=itmR.apply(strMethodName);
}








return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ApplyAfterAppGet' and [Method].is_current='1'">
<config_id>68D4F99209A644A5B27A09F73ED0B7F9</config_id>
<name>In_ApplyAfterAppGet</name>
<comments>呼叫物件的onAfterAppGet事件，由物件本體呼叫。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
