/*
目的:隱藏預算表的頁籤
做法:
1:如果目前的登入者是FA群組,則秀出全部的頁籤
*/
//debugger;


var sessionIdentitites_array=top.aras.getIdentityList().split(",");

if(top.aras.getIdentityList().indexOf("DE82487BA6B54B87A96F68417414938F",0)<0) //Finance Identity
{
   
	var tabs =  "In_budget_Income,In_Budget_parameter,In_Budget_RD_ActCost,In_Budget_Staff_ActCost,In_Budget_TimeRecords,In_Budget_TYLIN_Report";
	setTimeout("document.In_HideTabs('" + tabs + "');", 100);
}


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_HideBudgetTabs' and [Method].is_current='1'">
<config_id>C0D0A5B00C7941DBAE64EED10C3BC1FE</config_id>
<name>In_HideBudgetTabs</name>
<comments>隱藏預算表的頁籤</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
