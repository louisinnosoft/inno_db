//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

int intNumber = 0;

string aml = "";
string strObjectAttributeValue = "";

Item itmItemTypes = null;

//取得表頭物件
string strHeadObject = this.getProperty("HeadObject","");

//取得表頭條件
string strHeadCondition = this.getProperty("HeadCondition","");

//取得表頭內容
string strHeadContent = this.getProperty("HeadContent","");

//取得表身物件
string strBodyObject = this.getProperty("BodyObject","");

//取得屬性名稱
string strObjectAttributeName = this.getProperty("ObjectAttributeName","");

if(strBodyObject != ""){
    intNumber++;
}

//取得關連
for(int i=0;i<=intNumber;i++){
    aml = "<AML>";
    
    if(i==0){
        aml += "<Item type='"+strHeadObject+"' action='get'>";
        aml += "<"+strHeadCondition+">"+strHeadContent+"</"+strHeadCondition+">";
    }else{
        aml += "<Item type='"+strBodyObject+"' action='get'>";
        aml += "<source_id>"+itmItemTypes.getID()+"</source_id>";
    }
    aml += "</Item></AML>";
    itmItemTypes = inn.applyAML(aml);
}

//取得關係名稱
for(int i=0;i<itmItemTypes.getItemCount();i++){
	Item itmItemType = itmItemTypes.getItemByIndex(i);
	
	strObjectAttributeValue += itmItemType.getProperty(strObjectAttributeName,"") + ",";
}
strObjectAttributeValue = strObjectAttributeValue.Trim(',');

if(strObjectAttributeValue == ""){
    strObjectAttributeValue = "空白";
}

Item itm_R = inn.newItem("r");
itm_R.setProperty("ObjectAttributeValue",strObjectAttributeValue);

return itm_R;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_getObjectAttributeValue' and [Method].is_current='1'">
<config_id>81CA97118586410D852CDDEE7B1AC6B8</config_id>
<name>In_getObjectAttributeValue</name>
<comments>取得物件屬性值</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
