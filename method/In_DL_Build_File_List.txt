//System.Diagnostics.Debugger.Break();
/*
目的:抓到某個藥證下的所有交附文件清單(File List),累加至in_downloader
參數:<m_type>1:交付文件,2:欄位上文件</m_type><drug_lic_id></drug_lic_id><drug_lic_name></drug_lic_name><file_property></file_property>
作法:
*/

Innovator inn = this.getInnovator();
string aml = "";
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
	
string strType = this.getProperty("m_type","");
string strFile_Property = this.getProperty("file_property","");
string strDrug_Lic_Id = this.getProperty("drug_lic_id","");
string strDrug_Lic_Name = this.getProperty("drug_lic_name","");
string strDrug_Lic_Owner =  this.getProperty("drug_lic_owner","");


aml = "<AML>";
aml += "<Item type='in_downloader' action='add'>";
aml += "<name>藥證文件下載:" + strDrug_Lic_Name + "</name>";
aml += "<in_download_type>A</in_download_type>";
aml += "<owned_by_id>" + strDrug_Lic_Owner + "</owned_by_id>";
aml += "</Item>";
aml += "</AML>";

Item itmDownloader = inn.applyAML(aml);

switch(strType)
{
	case "1":
		aml = "<AML>";
		aml += "<Item type='In_Drug_Lic_Deliverable' action='get' select='related_id(" + strFile_Property + "(filename,file_size),in_approval_chapter(in_module,in_code))'>";
		aml += "<source_id>" + strDrug_Lic_Id + "</source_id>";
		aml += "</Item>";
		aml += "</AML>";
		
		Item itmDrug_Lic_Delis = inn.applyAML(aml);
		for(int i=0;i<itmDrug_Lic_Delis.getItemCount();i++)
		{
			Item itmDrug_Lic_Deli = itmDrug_Lic_Delis.getItemByIndex(i);
			if(itmDrug_Lic_Deli.getRelatedItem().getProperty(strFile_Property,"")=="")
				continue;
			Item itmFile = itmDrug_Lic_Deli.getRelatedItem().getPropertyItem(strFile_Property);
			Item itmDownloaderFileList = inn.newItem("In_downloader_filelist");
			double dbl_file_size = Convert.ToDouble(itmFile.getProperty("file_size","0"));
			dbl_file_size = dbl_file_size/1024/1024;
			itmDownloaderFileList.setProperty("in_filesize", dbl_file_size.ToString("###,##0.##"));
			
			string strFolder = "";
			if(itmDrug_Lic_Deli.getRelatedItem().getProperty("in_approval_chapter","")=="")
				strFolder = "";
			else
				strFolder = itmDrug_Lic_Deli.getRelatedItem().getPropertyItem("in_approval_chapter").getProperty("in_module","");
				
			itmDownloaderFileList.setProperty("in_all_foldername",strFolder);
			itmDownloaderFileList.setProperty("source_id",itmDownloader.getID());
			itmDownloaderFileList.setProperty("in_file_id",itmFile.getID());
			itmDownloaderFileList = itmDownloaderFileList.apply("add");			
		}
		
		break;
	default:
		break;
}



aml = "";
aml += "<AML>";
aml += "<Item type='In_Drug_Lic_downloader' action='add'>";
aml += "<related_id>" + itmDownloader.getID() + "</related_id>";
aml += "<source_id>" + strDrug_Lic_Id + "</source_id>";
aml += "</Item></AML>";

Item itmDL = inn.applyAML(aml);

//啟動轉檔
itmDownloader = itmDownloader.promote("Offline conversion",""); 

 if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
 
return itmDownloader;
	


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_DL_Build_File_List' and [Method].is_current='1'">
<config_id>24D982C60CDA426197840FAD4CE1C121</config_id>
<name>In_DL_Build_File_List</name>
<comments>inn drug</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
