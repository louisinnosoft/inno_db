var innovator = aras.newIOMInnovator();
var documentTypeId;
var elementTypeId;
var propertyTypeId;
var oldPermissionId;
var documentId;
var documentItemTypeId;
var documentItemTypeName;
var currentWindow;

var selectPermission = function(dialogResult) {
	if (!dialogResult) {
		return;
	}

	var doAutomigration = function(dialogResult) {
		if (!dialogResult) {
			return;
		}
		oldPermissionId = dialogResult.itemID;
		if (!oldPermissionId) {
			return;
		}

		var item = innovator.newItem('cmf_ContentType', '');
		item.setID(documentTypeId);
		item.setAttribute('method_action', 'UpdateDefaultPermission');
		item.setProperty('property_id', propertyTypeId);
		item.setProperty('element_id', elementTypeId);
		item.setProperty('document_id', documentId);
		item.setProperty('old_permission_id', oldPermissionId);
		item = item.apply('cmf_SchemaDefinitionHandler');

		if (item.isError()) {
			aras.AlertError(item);
		} else {
			aras.AlertSuccess(aras.getResource('../Modules/aras.innovator.CMF/', 'cmf_automigration_success'));
		}
	};

	if (!documentId) {
		// update from property type
		documentId = dialogResult.itemID;
	} /*else {
		// update from document item
		propertyTypeId = dialogResult.itemID;

		var propertyTypeItem = innovator.newItem('cmf_PropertyType', 'get');
		propertyTypeItem.setAttribute('select', 'source_id');
		propertyTypeItem.setID(propertyTypeId);
		var elementTypeItem = innovator.newItem('cmf_ElementType', 'get');
		elementTypeItem.setAttribute('select', 'source_id');
		propertyTypeItem.setPropertyItem('source_id', elementTypeItem);
		propertyTypeItem = propertyTypeItem.apply();
		elementTypeItem = propertyTypeItem.getPropertyItem('source_id');
		if (documentTypeId != elementTypeItem.getProperty('source_id')) {
			aras.AlertError(aras.getResource('../Modules/aras.innovator.CMF/', 'cmf_migrate_permissions_different_documents'));
		}
		elementTypeId = elementTypeItem.getID();
	}*/

	aras.AlertWarning(aras.getResource('../Modules/aras.innovator.CMF/', 'cmf_migrate_permissions_select_permission'));

	var params = {};
	params.aras = aras;
	params.itemtypeName = 'Permission';
	params.multiselect = false;
	params.type = 'SearchDialog';
	setTimeout(function() {
		currentWindow.ArasModules.MaximazableDialog.show('iframe', params).promise.then(doAutomigration);
	}, 100);
};

if (this.getType() == 'cmf_PropertyType') {
	// update from property type
	propertyTypeId = this.getID();
	elementTypeId = this.getProperty('source_id');
	currentWindow = aras.getMostTopWindowWithAras(window);

	var elementTypeItem = innovator.newItem('cmf_ElementType', 'get');
	elementTypeItem.setAttribute('select', 'source_id');
	elementTypeItem.setID(elementTypeId);
	var contentTypeItem = innovator.newItem('cmf_ContentType', 'get');
	contentTypeItem.setAttribute('select', 'linked_item_type');
	elementTypeItem.setPropertyItem('source_id', contentTypeItem);
	elementTypeItem = elementTypeItem.apply();

	contentTypeItem = elementTypeItem.getPropertyItem('source_id');
	documentTypeId = contentTypeItem.getID();
	documentItemTypeId = contentTypeItem.getProperty('linked_item_type');
	documentItemTypeName = aras.getItemTypeName(documentItemTypeId);

	var allPromt = aras.getResource('../Modules/aras.innovator.CMF/', 'cmf_migrate_all_permissions');
	if (aras.confirm(allPromt)) {
		selectPermission({'itemID': null});
	} else {
		var params = {};
		params.aras = aras;
		params.itemtypeName = documentItemTypeName;
		params.multiselect = false;
		params.type = 'SearchDialog';
		currentWindow.ArasModules.MaximazableDialog.show('iframe', params).promise.then(selectPermission);
	}
} /*else {
	// update from document item
	documentItemTypeName = this.getType();
	documentItemTypeId = aras.getItemTypeId(documentItemTypeName);
	documentId = this.getID();
	currentWindow = aras.getMainWindow().main;

	var contentTypeItem = innovator.newItem('cmf_ContentType', 'get');
	contentTypeItem.setAttribute('select', 'id');
	contentTypeItem.setProperty('linked_item_type', documentItemTypeId);
	contentTypeItem = contentTypeItem.apply();
	documentTypeId = contentTypeItem.getID();

	var params = {};
	params.aras = aras;
	params.itemtypeName = 'cmf_PropertyType';
	params.multiselect = false;
	params.type = 'SearchDialog';
	currentWindow.ArasModules.MaximazableDialog.show('iframe', params).promise.then(selectPermission);
}*/

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_UpdatePermissions' and [Method].is_current='1'">
<config_id>DA952ADAA54241328424804DC465F19E</config_id>
<name>cmf_UpdatePermissions</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
