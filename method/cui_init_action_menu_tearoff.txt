var canExecute = true;
if (inArgs.control && inArgs.control.additionalData) {
	var additionalData = JSON.parse(inArgs.control.additionalData);
	canExecute = aras.canInvokeActionImpl(additionalData.canExecuteMethodName, additionalData.location);
}

return {'cui_disabled': !canExecute};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_init_action_menu_tearoff' and [Method].is_current='1'">
<config_id>723C33B7F24B4523A6D1118AC993CDF9</config_id>
<name>cui_init_action_menu_tearoff</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
