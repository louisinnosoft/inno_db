/*
In_QuoteToProject
	目的:將報價項目轉成專案任務Array,之後透過In_CreateProject產生對應的專案
	做法:
	1.先建立一個root element, current_Lev = 0, parent_id[current_Lev(0)] 等於 root element id, current_Lev = 0
	2.依據sort_order依序取得所有的 bcs_QuoteDetail
	3.若 In_Lv 欄位 為 1: (Header)
	-->current_Lev = 1, current_wbs 為自己
	-->建立一個 wbs element , parent_id 為 L0_parent_id
	-->parent_id[current_Lev(1)] 設為自己
	4.若 In_Lv 為2,
	4-1:建立一個 wbs element, 
	---->current_act2=""
	---->parent_id[current_Lev]設為自己
	---->prev_item = current_wbs
	---->current_wbs 為自己
	5.若 In_Lv 為3,
	5.1:找到 related_id 的 part 的 in_act2_type, 
	-->若為wbs element
	---->建立一個 wbs element, current_act2=""
	---->parent_id[In_Lv]設為自己
	---->prev_item = current_wbs, parent_id=parent_id[In_Lv-1]
	---->current_wbs 為自己
	---->找他的 PartBOM
	------>找到屬於 Activity2 的 Part
	------>prev_item = current_act2, parent_id = current_wbs
	------>current_act2 = 自己
	5-2:如果是 Activity2, 則建立一個 Activity2
	---->prev_item = current_act2, parent_id = current_wbs
	---->current_act2 = 自己
	*/
	//System.Diagnostics.Debugger.Break();
//2018-02-27: 報價轉專案時,會將owner填入專案的owner

	string QuoteId = this.getProperty("QuoteId");
	Innovator inn = this.getInnovator();

	Item itmQuote = inn.getItemById("In_Quote", QuoteId);

	string aml = "";
	string strPrjName = itmQuote.getProperty("in_name", "");
	string[] parent_id = new string[5];
	string[] prev_WBS_id = new string[5]; //各階層的pre_item id, 可以是 wbs_id也可以是 activity2_id


	string strProj_Num = "";
	Item itmProject = null;
	Item itmRootWBS = null;
	string strTeamRoles = "";

	bool IsAddNew = false; //是否為新增專案
	string sql = "";
	string strEpisodes = ""; //一階名:集數,一階名:集數,...
	System.Collections.Hashtable htEpisodes = new System.Collections.Hashtable(); //創建一個Hashtable實例

	try
	{
		//2.依據sort_order依序取得所有的 In_QuoteDetail
		aml = "<AML>";
		aml += "<Item type='In_Quote_Details' action='get' orderBy='sort_order'>";
		aml += "<source_id>" + itmQuote.getID() + "</source_id>";
		aml += "</Item></AML>";

		Item itmQuote_Details = inn.applyAML(aml);

		if (itmQuote_Details.isError())
			throw new Exception("沒有報價子項無法轉出專案任務");

		//先判斷是否有已存在的專案
		aml = "<AML>";
		aml += "<Item type='Project' action='get'>";
		aml += "<in_ref_quote>" + itmQuote.getID() + "</in_ref_quote>";
		aml += "</Item></AML>";

		itmProject = inn.applyAML(aml);
		if (itmProject.isError())
		{
			IsAddNew = true;
			//itmProject = QuoteToExistProject(QuoteId, itmProject.getItemByIndex(0));
			//return itmProject;

			//1.先建立一個root element,  parent_id[current_Lev(0)] 等於 root element id,
			itmRootWBS = inn.newItem("WBS Element", "add");
			itmRootWBS.setProperty("name", strPrjName);
			itmRootWBS.setProperty("is_top", "1");
			itmRootWBS = itmRootWBS.apply();


			//strCurrentWBSId = itmRootWBS.getID();

			itmProject = inn.newItem("Project", "add");
			itmProject.setProperty("date_start_target", DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss"));
			itmProject.setProperty("date_due_target", DateTime.Now.AddMonths(2).ToString("yyyy-MM-ddThh:mm:ss"));
			itmProject.setProperty("project_number", inn.getNextSequence("Project Number"));
			itmProject.setProperty("scheduling_type", "Forward");
			itmProject.setProperty("scheduling_method", "7DC85B0668134E949B9212D7CE199265");
			itmProject.setProperty("update_method", "6E1133AB87A44D529DF5F9D1FD740100");
			itmProject.setProperty("scheduling_mode", "1");
			itmProject.setProperty("project_update_mode", "1");
			itmProject.setProperty("name", strPrjName);
			itmProject.setProperty("wbs_id", itmRootWBS.getID());
			itmProject.setProperty("in_ref_quote", itmQuote.getID());
			itmProject.setProperty("in_number", itmQuote.getProperty("item_number", ""));
			itmProject.setProperty("owned_by_id", itmQuote.getProperty("owned_by_id",""));
			
			itmProject = itmProject.apply();


		}
		else
		{
			if (itmProject.getItemCount() > 1)
				throw new Exception("目前有找到兩個以上的專案都使用到本報價單,請修正本錯誤");

			itmRootWBS = inn.getItemById("WBS Element", itmProject.getProperty("wbs_id"));
		}
		parent_id[0] = itmRootWBS.getID();
		strProj_Num = itmProject.getProperty("project_number");
		Item itmPrevWBS = null;
		Item itmSectionQuoteDetails = null;

		//先抓出有幾個第一階,每個第一階各有幾集
		string strRepeat = "0";
		string strLastLevel1Id = "";
		string strLastLevel1CfgId = "";
		string strLevel1Name = "";
		for (int i = 0; i < itmQuote_Details.getItemCount(); i++)
		{
			Item itmQuote_Detail = itmQuote_Details.getItemByIndex(i);
			string strLevel = itmQuote_Detail.getProperty("in_level", "0");
			switch (strLevel)
			{
				case "0":
					break;
				case "1":
					if (strLastLevel1Id != "")
					{
						//代表是第二個以後的Level1,代表前一組第一階任務已經抓取完畢了,所以要放進去建立樹狀任務專案
						//itmPrevWBS = CreateWBS(strProj_Num,itmRootWBS,itmPrevWBS,itmSectionQuoteDetails,strLevel1Name, Convert.ToInt32(strRepeat));
						itmPrevWBS = MergeWBS(strProj_Num, itmRootWBS, itmPrevWBS, itmSectionQuoteDetails, strLevel1Name, Convert.ToInt32(strRepeat));
						strEpisodes = Innosoft.InnUtility.PushUniqueQueue(strEpisodes, strLevel1Name + ":" + strRepeat, ",");
						//htEpisodes.Add(strLastLevel1Id, itmSectionQuoteDetails);
						htEpisodes.Add(strLastLevel1CfgId, itmSectionQuoteDetails);

					}
					strLastLevel1Id = itmQuote_Detail.getID();
					strLastLevel1CfgId = itmQuote_Detail.getProperty("in_config_id", "");
					strRepeat = itmQuote_Detail.getProperty("in_qty", "1");
					strLevel1Name = itmQuote_Detail.getProperty("in_part_name", "");
					itmSectionQuoteDetails = itmQuote_Detail; //重新開始組合新的影片任務                            
					break;
				default:
					itmSectionQuoteDetails.appendItem(itmQuote_Detail);
					break;
			}
		}
		itmPrevWBS = MergeWBS(strProj_Num, itmRootWBS, itmPrevWBS, itmSectionQuoteDetails, strLevel1Name, Convert.ToInt32(strRepeat));
		//itmPrevWBS = CreateWBS(strProj_Num, itmRootWBS, itmPrevWBS, itmSectionQuoteDetails, strLevel1Name, Convert.ToInt32(strRepeat));
		strEpisodes = Innosoft.InnUtility.PushUniqueQueue(strEpisodes, strLevel1Name + ":" + strRepeat, ",");
		//htEpisodes.Add(strLevel1Name, itmSectionQuoteDetails);
		htEpisodes.Add(strLastLevel1CfgId, itmSectionQuoteDetails);


		if (!IsAddNew)
		{
			//如果是更新專案,則需要比對報價單與專案任務

			/*集數減少的作法:從大的後面往前砍WBS,
			如果該WBS的專案任務都是pending狀態,則將整個一階WBS全砍掉,否則就是全都變成complete
			可以透過 strEpisodes 判斷本次的集數是否有減少

			1.抓到所有一階的WBS,然後比對差異

			*/


			sql = "SELECT  IN_QUOTE_DETAILS.IN_CONFIG_ID as QuoteDetails_Id,IN_QUOTE_DETAILS.IN_QTY,WBS_ELEMENT.IN_EPISODE";
			sql += " FROM    IN_QUOTE_DETAILS INNER JOIN WBS_ELEMENT ON ";
			sql += "        IN_QUOTE_DETAILS.IN_CONFIG_ID = WBS_ELEMENT.IN_QUOTE_DETAIL_CFGID";
			sql += " WHERE   (IN_QUOTE_DETAILS.IN_LEVEL = 1) AND ";
			sql += "        (IN_QUOTE_DETAILS.SOURCE_ID = '" + QuoteId + "')		";
			sql += " AND (innovator.WBS_ELEMENT.IN_EPISODE > innovator.IN_QUOTE_DETAILS.IN_QTY)";
			sql += " ORDER BY   IN_QUOTE_DETAILS.IN_PART_NAME, WBS_ELEMENT.IN_EPISODE DESC";
			Item itmDiffLev1WBSs = inn.applySQL(sql);


			string[] arrEpisodes = strEpisodes.Split(',');
			string strPreQuoteCfgId = "";
			for (int i = 0; i < itmDiffLev1WBSs.getItemCount(); i++)
			{
				Item itmDiffLev1WBS = itmDiffLev1WBSs.getItemByIndex(i);
				if (strPreQuoteCfgId != itmDiffLev1WBS.getProperty("quotedetails_id", ""))
				{
					//先計算原本的WBS集數比最新報價的集數少了幾筆
					int intWBSEpisodes = int.Parse(itmDiffLev1WBS.getProperty("in_episode", "0"));
					int intQuoteDetailEpisodes = int.Parse(itmDiffLev1WBS.getProperty("in_qty", "0"));
					int intDiff = intWBSEpisodes - intQuoteDetailEpisodes;
					itmSectionQuoteDetails = (Item)htEpisodes[itmDiffLev1WBS.getProperty("quotedetails_id", "")];
					for (int j = 1; j <= intDiff; j++)
					{
						for (int k = 0; k < itmSectionQuoteDetails.getItemCount(); k++)
						{
							//遍歷同一個一階下面的所有報價項
							Item itmSectionQuoteDetail = itmSectionQuoteDetails.getItemByIndex(k);
							switch (itmSectionQuoteDetail.getProperty("in_level"))
							{
								case "1":
									DisableProjectElements(itmSectionQuoteDetail.getProperty("in_config_id", ""), itmProject.getProperty("project_number", ""), "WBS Element", intQuoteDetailEpisodes + j);
									break;
								case "2":
									DisableProjectElements(itmSectionQuoteDetail.getProperty("in_config_id", ""), itmProject.getProperty("project_number", ""), "WBS Element", intQuoteDetailEpisodes + j);
									//DisableProjectElements(itmSectionQuoteDetail.getProperty("in_config_id", ""), itmProject.getProperty("project_number", ""), "Activity2", intQuoteDetailEpisodes + j);
									break;
								case "3":
									DisableProjectElements(itmSectionQuoteDetail.getProperty("in_config_id", ""), itmProject.getProperty("project_number", ""), "Activity2", intQuoteDetailEpisodes + j);
									break;
								default:
									break;
							}

						}
					}


				}
				strPreQuoteCfgId = itmDiffLev1WBS.getProperty("quotedetails_id", "");

			}








			//將被刪除的報價項所對應的Act2變成Closed
			//取得所有報價品項的in_config_id
			sql = "select (";
			sql += "SELECT Distinct in_config_id + ',' AS 'data()'   FROM ";
			sql += "[In_Quote_Details]  where source_id='" + QuoteId + "' FOR XML PATH ('')";
			sql += ") as c1";
			Item itmDetailConfigIDs = inn.applySQL(sql);
			string strDetailConfigIDs = itmDetailConfigIDs.getProperty("c1", "");
			strDetailConfigIDs = strDetailConfigIDs.Replace(", ", ",");
			strDetailConfigIDs = strDetailConfigIDs.Trim(',');


			//先取得所有的 Activity2的QuoteDetails的in_config_id
			sql = "select (";
			sql += "SELECT Distinct in_quote_detail_cfgid + ',' AS 'data()'   FROM ";
			sql += "[Activity2]  where proj_num='" + itmProject.getProperty("project_number", "") + "' FOR XML PATH ('')";
			sql += ") as c1";
			Item itmAct2DetailIDs = inn.applySQL(sql);
			string strAct2DetailIDs = itmAct2DetailIDs.getProperty("c1", "");
			strAct2DetailIDs = strAct2DetailIDs.Replace(", ", ",");
			strAct2DetailIDs = strAct2DetailIDs.Trim(',');

			sql = "select (";
			sql += "SELECT Distinct in_quote_detail_cfgid + ',' AS 'data()'   FROM ";
			sql += "[WBS_Element]  where proj_num='" + itmProject.getProperty("project_number", "") + "' FOR XML PATH ('')";
			sql += ") as c1";
			Item itmWBSQuoteDetailIDs = inn.applySQL(sql);
			string strWBSQuoteDetailIDs = itmWBSQuoteDetailIDs.getProperty("c1", "");
			strWBSQuoteDetailIDs = strWBSQuoteDetailIDs.Replace(", ", ",");
			strWBSQuoteDetailIDs = strWBSQuoteDetailIDs.Trim(',');


			//專案有報價單無(刪減項)
			string strPendingAct2IDs = ""; //需Pending的任務項
			string[] arrDetailConfigIDs = strDetailConfigIDs.Split(',');
			string[] arrAct2DetailIDs = strAct2DetailIDs.Split(',');
			for (int i = 0; i < arrAct2DetailIDs.Length; i++)
			{
				if (!strDetailConfigIDs.Contains(arrAct2DetailIDs[i]))
				{
					strPendingAct2IDs = Innosoft.InnUtility.PushUniqueQueue(strPendingAct2IDs, arrAct2DetailIDs[i], ",");
					DisableProjectElements(arrAct2DetailIDs[i], itmProject.getProperty("project_number", ""), "Activity2", 0);
				}
			}


			//string strPendingWBSIDs = ""; //需Pending的WBS
			string[] arrWBSQuoteDetailIDs = strWBSQuoteDetailIDs.Split(',');
			for (int i = 0; i < arrWBSQuoteDetailIDs.Length; i++)
			{
				if (!strDetailConfigIDs.Contains(arrWBSQuoteDetailIDs[i]))
				{
					//strPendingWBSIDs = InnUtility.PushUniqueQueue(strPendingWBSIDs, arrWBSQuoteDetailIDs[i], ",");
					DisableProjectElements(arrWBSQuoteDetailIDs[i], itmProject.getProperty("project_number", ""), "WBS Element", 0);

				}
			}

			//處理不存在於報價單的 WBS Element
			//把X-開頭的WBS都移到作廢區內
			/*
			 * 
			 */




			//因為新舊WBS Element的 prev_item 都連到同一個 WBS才會壞掉,所以將新的WBS的prev_item接到舊wbs即可

			//sql = "select count(prev_item) as c1,prev_item from innovator.WBS_ELEMENT where proj_num='" + itmProject.getProperty("project_number", "") + "' and prev_item<>''  group by prev_item having count(prev_item)>1";
			//Item itmConfuseWBSs = inn.applySQL(sql);
			//if(!itmConfuseWBSs.isError())
			//{
			//    for(int i=0; i< itmConfuseWBSs.getItemCount();i++)
			//    {
			//        Item itmConfuseWBS = itmConfuseWBSs.getItemByIndex(i);
			//        string strPrevItem = itmConfuseWBS.getProperty("prev_item", "");
			//        sql = "select * from innovator.WBS_ELEMENT where proj_num='" + itmProject.getProperty("project_number", "") + "' and prev_item='" + strPrevItem + "'";
			//        Item itmConfuseWBS1s = inn.applySQL(sql);
			//        string strNewWBSPrevItem = "";

			//        //一定要先找到不存在於報價單的WBS的ID才可以
			//        for (int j=0;j< itmConfuseWBS1s.getItemCount();j++)
			//        {
			//            Item itmConfuseWBS1 = itmConfuseWBS1s.getItemByIndex(j);
			//            if(!strDetailConfigIDs.Contains(itmConfuseWBS1.getProperty("in_quote_detail_cfgid","")))
			//            {
			//                //找到不存在於報價單的WBS，記下他的id, 以便之後當別人的prev_item
			//                strNewWBSPrevItem = itmConfuseWBS1.getID();
			//            }                               
			//        }

			//        for (int j = 0; j < itmConfuseWBS1s.getItemCount(); j++)
			//        {
			//            Item itmConfuseWBS1 = itmConfuseWBS1s.getItemByIndex(j);
			//            if (strDetailConfigIDs.Contains(itmConfuseWBS1.getProperty("in_quote_detail_cfgid", "")))
			//            {                                  
			//                //更新正常的WBS的prev_item
			//                sql = "Update [WBS_Element] set prev_item='" + strNewWBSPrevItem + "' where id='" + itmConfuseWBS1.getID() + "'";
			//                inn.applySQL(sql);
			//                strNewWBSPrevItem = itmConfuseWBS1.getID();
			//            }
			//        }

			//    }
			//}
			//*/


		}


		itmProject = itmProject.apply("edit");

		sql = "Update [In_Quote] set in_project='" + itmProject.getID() + "' where id='" + itmQuote.getID() + "'";
		itmQuote = inn.applySQL(sql);

		sql = "select role from [Project_Team] where source_id='" + itmProject.getID() + "'";
		Item itmExistRoles = inn.applySQL(sql);
		string strExistRoles = "";
		for (int i = 0; i < itmExistRoles.getItemCount(); i++)
		{
			strExistRoles += "'" + itmExistRoles.getItemByIndex(i).getProperty("role") + "',";
		}
		strExistRoles = strExistRoles.TrimEnd(',');
		sql = "select Distinct lead_role from [Activity2] where proj_num='" + strProj_Num + "'";
		if (strExistRoles != "")
			sql += " and lead_role not in (" + strExistRoles + ")";
		Item itmLeaderRoles = inn.applySQL(sql);
		for (int i = 0; i < itmLeaderRoles.getItemCount(); i++)
		{
			Item itmLeaderRole = itmLeaderRoles.getItemByIndex(i);
			if (itmLeaderRole.getProperty("lead_role", "") != "")
			{
				aml = "<AML>";
				aml += "<Item type='Project Team' action='add'>";
				aml += "<source_id>" + itmProject.getID() + "</source_id>";
				aml += "<role>" + itmLeaderRole.getProperty("lead_role", "") + "</role>";
				aml += "</Item></AML>";

				Item itmPT = inn.applyAML(aml);
			}
		}

		//處理將1階的名稱押到activity上
		aml  = "<AML>";
        aml += "<Item type='In_Quote_Details' action='get' orderBy='sort_order'>";
        aml += "<source_id>"+QuoteId+"</source_id>";
        aml += "<in_level condition='in'>1,3</in_level>";
        aml += "</Item></AML>";
        Item itmQuoteDetails = inn.applyAML(aml);
		string strPartName="";
		sql="";
        for(int j=0;j<itmQuoteDetails.getItemCount();j++)
        {
            if(itmQuoteDetails.getItemByIndex(j).getProperty("in_level","") == "1")
            {
                strPartName = itmQuoteDetails.getItemByIndex(j).getProperty("in_part_name","");
            }
            else
            {
                aml  = "<AML>";
                aml += "<Item type='Activity2' action='get'>";
                aml += "<proj_num>"+itmProject.getProperty("project_number","")+"</proj_num>";
                aml += "<in_quote_detail_cfgid>"+itmQuoteDetails.getItemByIndex(j).getProperty("in_config_id","")+"</in_quote_detail_cfgid>";
                aml += "</Item></AML>";
                Item itmActivity2 = inn.applyAML(aml);

                for(int k=0;k<itmActivity2.getItemCount();k++)
                {
                    sql += "UPDATE Activity2 SET name = N'" + strPartName + " / " + itmActivity2.getItemByIndex(k).getProperty("name","") + "' WHERE ID = '" + itmActivity2.getItemByIndex(k).getID() + "'" + "\n";
                }
            }
        }
		 inn.applySQL(sql);



	}
	catch (Exception ex)
	{
		string strError = (ex.InnerException == null ? ex.Message : ex.InnerException.Message);
		string strErrorDetail = "";
		strErrorDetail = strError + "\n" + ex.ToString();
		Innosoft.InnUtility.AddLog(strErrorDetail, "Fatal");
		throw new Exception(strError);
	}

	return itmProject;

}

private Item DisableProjectElements(string in_quote_detail_cfgid, string proj_num, string ProjectElementType, int Episode)
{
	Innovator inn=this.getInnovator();
	Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
	string aml = "";
	string str_r = "";
	try
	{


		aml = "<AML>";
		aml += "<Item type='" + ProjectElementType + "' action='get'>";
		aml += "<in_quote_detail_cfgid>" + in_quote_detail_cfgid + "</in_quote_detail_cfgid>";
		aml += "<proj_num>" + proj_num + "</proj_num>";
		aml += "<state condition='ne'>Complete</state>";
		if (Episode.ToString() != "0")
			aml += "<in_episode>" + Episode.ToString() + "</in_episode>";
		aml += "<state condition='ne'>Complete</state>";
		aml += "</Item></AML>";
		Item ProjectElements = inn.applyAML(aml);
		for (int j = 0; j < ProjectElements.getItemCount(); j++)
		{
			Item ProjectElement = ProjectElements.getItemByIndex(j);
			str_r += ProjectElement.getID() + ",";
			if (ProjectElementType == "Activity2")
				ProjectElement.setProperty("percent_compl", "100");

			if (!ProjectElement.getProperty("name").StartsWith("xx-"))
				ProjectElement.setProperty("name", "xx-" + ProjectElement.getProperty("name"));

			ProjectElement = ProjectElement.apply("edit");

			if (ProjectElementType == "Activity2")
			{

				string sql = "delete from [Predecessor] where related_id='" + ProjectElement.getID() + "'";
				inn.applySQL(sql);

				sql = "delete from [Predecessor] where source_id='" + ProjectElement.getID() + "'";
				inn.applySQL(sql);

				if (ProjectElement.getProperty("state") == "Active" || ProjectElement.getProperty("state") == "Pending")
					ProjectElement = ProjectElement.promote("Complete", "取消報價項目");



			}


		}
		str_r = str_r.TrimEnd(',');
	}
	catch (Exception ex)
	{
		string strError = (ex.InnerException == null ? ex.Message : ex.InnerException.Message);
		string strErrorDetail = "";
		strErrorDetail = strError + "\n" + ex.ToString();
		//Innosoft.InnUtility.AddLog(strErrorDetail, "Fatal", System.Reflection.MethodBase.GetCurrentMethod());
		throw new Exception(strError);
	}
	return inn.newResult(_InnH.Translate(str_r));
}

 private Item MergeWBS(string Proj_Num, Item ParentWBS, Item PrevItem, Item QuoteDetails, string Name, int RepeatTimes)
{
	Innovator inn=this.getInnovator();
	string aml = "";
	string[] parent_id = new string[5];
	string[] prev_WBS_id = new string[5]; //各階層的pre_item id, 可以是 wbs_id也可以是 activity2_id


	string strCurrentWBSId = "";
	string strCurrentAct2Id = "";
	string strPredecessorAct2Id = "";


	string strWBSName = "";

	Item itmPredecessor;
	Item itmSubWBS;
	int intLevel = 0;


	Item itmAct2 = null;
	Item itmWBSAct2;
	Item itmAct2s;
	Item itmWBSAct2s;
	Item itmWBSs = null;
	Item itmWBS = null;
	Item itmThisRootWBS = null; //要回傳的WBS

	string strLev1QuoteDetailCfg_Id = "";


	int intStartIndex = 0; //如果是沒有重複,則從1開始找,如果是有重複,則從0開始抓



	string sql = "";
	parent_id[0] = ParentWBS.getID();
	prev_WBS_id[1] = (PrevItem == null) ? "" : PrevItem.getID();

	try
	{
		for (int z = 1; z <= RepeatTimes; z++)
		{
			string strLev2WBSIds = "";
			for (int i = intStartIndex; i < QuoteDetails.getItemCount(); i++)
			{
				Item itmQuote_Detail = QuoteDetails.getItemByIndex(i);
				//Item itmPart = itmQuote_Detail.getRelatedItem();

				string strLevel = itmQuote_Detail.getProperty("in_level", "0");
				intLevel = Int32.Parse(strLevel);

				switch (strLevel)
				{
					case "0":
						break;
					case "1":
					case "2":
						//case "3":
						//3.若 In_Lv 欄位 為 1: (Header)
						//-->current_Lev = 1, current_wbs 為自己
						//-->建立一個 wbs element , parent_id 為 L0_parent_id
						//-->parent_id[current_Lev(1)] 設為自己

						prev_WBS_id[intLevel + 1] = "";
						//比對自己是否出現在WBS 內                        
						//sql = "select * from [WBS_Element] where in_quote_detail_cfgid='" + itmQuote_Detail.getProperty("in_config_id") + "' and proj_num='" + Proj_Num + "'";
						//不能用SQL Select, 因為 不會return資料
						aml = "<AML>";
						aml += "<Item type='WBS Element' action='get'>";
						aml += "<in_quote_detail_cfgid>" + itmQuote_Detail.getProperty("in_config_id") + "</in_quote_detail_cfgid>";
						aml += "<proj_num>" + Proj_Num + "</proj_num>";
						aml += "<in_episode>" + z.ToString() + "</in_episode>";
						aml += "</Item></AML>";
						itmWBSs = inn.applyAML(aml);
						if (itmWBSs.getItemCount() <= 0)
						{
							strWBSName = itmQuote_Detail.getProperty("in_part_name", "");
							if (RepeatTimes > 1 && strLevel == "1")
							{
								strWBSName = strWBSName + "(" + z.ToString() + ")";
							}
							itmWBS = inn.newItem("WBS Element", "add");
							itmWBS.setProperty("name", strWBSName);
							itmWBS.setProperty("prev_item", (prev_WBS_id[intLevel] == null) ? "" : prev_WBS_id[intLevel]);
							itmWBS.setProperty("proj_num", Proj_Num);
							itmWBS.setProperty("in_episode", z.ToString());
							itmWBS.setProperty("in_quote_detail_cfgid", itmQuote_Detail.getProperty("in_config_id", ""));
							itmWBS = itmWBS.apply();

							aml = "<AML>";
							aml += "<Item type='Sub WBS' action='add'>";
							aml += "<source_id>" + parent_id[intLevel - 1] + "</source_id>";
							aml += "<related_id>" + itmWBS.getID() + "</related_id>";
							aml += "</Item></AML>";
							itmSubWBS = inn.applyAML(aml);
						}
						else
						{
							for (int k = 0; k < itmWBSs.getItemCount(); k++)
							{
								itmWBS = itmWBSs.getItemByIndex(k);
								strWBSName = itmQuote_Detail.getProperty("in_part_name", "");
								if (RepeatTimes > 1 && strLevel == "1")
								{
									if (!strWBSName.EndsWith("(" + z.ToString() + ")"))
										strWBSName = strWBSName + "(" + z.ToString() + ")";
								}
								//itmWBS.setAction("edit");
								// itmWBS.setProperty("name", strWBSName);
								string strPreItem = (prev_WBS_id[intLevel] == null) ? "" : prev_WBS_id[intLevel];


								//itmWBS = itmWBS.apply();
								sql = "Update [WBS_Element] set [name]=N'" + strWBSName + "',prev_item='" + strPreItem + "' where id='" + itmWBS.getID() + "'";
								Item itmWBS_R = inn.applySQL(sql);


							}
						}
						strCurrentWBSId = itmWBS.getID();
						parent_id[intLevel] = itmWBS.getID();
						prev_WBS_id[intLevel] = itmWBS.getID();
						strCurrentAct2Id = ""; // 每到一個新的WBS,就歸零
						if (strLevel == "1")
						{
							strPredecessorAct2Id = ""; //每到了第一階就抹除前置任務                                
							//if (itmThisRootWBS == null)
							itmThisRootWBS = itmWBS;
							strLev1QuoteDetailCfg_Id = itmWBS.getProperty("in_quote_detail_cfgid", "");
						}
						else
						{
							strLev2WBSIds += itmWBS.getID() + ",";
						}

						
						break;
					case "3":
						//若為階層三,則為專案任務

						double dblLev3Qty = Convert.ToDouble(itmQuote_Detail.getProperty("in_qty", "0"));
						string strLev3ConfigId = itmQuote_Detail.getProperty("in_config_id", "0");

						string strLev3Note1 = itmQuote_Detail.getProperty("in_note1", "");
						string strLev3Note2 = itmQuote_Detail.getProperty("in_note2", "");
						Item itmPart = inn.getItemById("Part", itmQuote_Detail.getProperty("related_id"));

						aml = "<AML>";
						aml += "<Item type='Activity2' action='get'>";
						aml += "<in_quote_detail_cfgid>" + strLev3ConfigId + "</in_quote_detail_cfgid>";
						aml += "<proj_num>" + Proj_Num + "</proj_num>";
						aml += "<in_episode>" + z.ToString() + "</in_episode>";
						aml += "</Item></AML>";
						itmAct2 = inn.applyAML(aml);
						if (itmAct2.isError())
						{
							//代表原本沒有對應的專案任務

							//創建一個虛擬的itmQuote_Detail
							Item fakeQuote_Detail = inn.newItem("In_Quote_Detail");
							//double dblQty = dblLev3Qty * Convert.ToDouble(itmPartBom.getProperty("qty", "1"));
							fakeQuote_Detail.setProperty("in_qty", dblLev3Qty.ToString());

							//double dblUnitWork = dblQty * Convert.ToDouble(itmPart.getProperty("in_unit_work", "1"));
							fakeQuote_Detail.setProperty("in_part_unit_work", itmPart.getProperty("in_unit_work", "1"));
							fakeQuote_Detail.setProperty("in_part_lead_role", itmPart.getProperty("in_lead_role", ""));
							fakeQuote_Detail.setProperty("in_part_leader", itmPart.getProperty("owned_by_id", ""));
							fakeQuote_Detail.setProperty("in_config_id", strLev3ConfigId);
							fakeQuote_Detail.setProperty("in_part_name", itmQuote_Detail.getProperty("in_part_name", ""));
							fakeQuote_Detail.setProperty("in_note1", strLev3Note1);
							fakeQuote_Detail.setProperty("in_note2", strLev3Note2);


							//就把自己當成專案任務階
							itmAct2 = inn.newItem("Activity2", "add");
							itmAct2 = MergeQuoteDetailToActivity2(
										fakeQuote_Detail
										, itmAct2
										, fakeQuote_Detail.getProperty("in_part_name", "")
										, strCurrentAct2Id
										, Proj_Num
										, z.ToString()
										, strCurrentWBSId
										, strPredecessorAct2Id);
							strCurrentAct2Id = itmAct2.getID();
							//prev_WBS_id[intLevel] = strCurrentAct2Id;
							strPredecessorAct2Id = strCurrentAct2Id;
						}

						else
						{
							//代表已有專案任務
							strCurrentAct2Id = itmAct2.getID();
							//prev_WBS_id[intLevel] = strCurrentAct2Id;
							strPredecessorAct2Id = strCurrentAct2Id;
							if (itmAct2.getProperty("name", "") != itmQuote_Detail.getProperty("in_part_name", "") ||
								itmAct2.getProperty("in_note1", "") != itmQuote_Detail.getProperty("in_note1", "") ||
								 itmAct2.getProperty("in_note2", "") != itmQuote_Detail.getProperty("in_note2", ""))
							{
								sql = "Update [Activity2] set ";
								sql += " [name]=N'" + itmQuote_Detail.getProperty("in_part_name", "") + "' ";
								sql += ",in_note1=N'" + itmQuote_Detail.getProperty("in_note1", "") + "'";
								sql += ",in_note2=N'" + itmQuote_Detail.getProperty("in_note2", "") + "'";
								sql += " where id='" + itmAct2.getID() + "'";
								itmAct2 = inn.applySQL(sql);
							}

						}




						break;
					default:
						break;

				}

			}
			//要處理原本在專案中的第二階WBS，但已不存在於新版報價單的報價品項
			/*做法:
			 * 1.透過 in_quote_detail_cfgid 找出被刪除的 WBS
			 * 2.檢查是否有「已作廢WBS」的WBS,如果沒有則創建一個
			 * 3.將所有被刪除的WBS都移到已作廢WBS內
			*/
			strLev2WBSIds = strLev2WBSIds.Trim(',');
			if (strLev2WBSIds != "")
			{
				//2.檢查是否有「已作廢WBS」的WBS
				aml = "<AML>";
				aml += "<Item type='WBS Element' action='get'>";
				aml += "<in_episode>" + z.ToString() + "</in_episode>";
				aml += "<in_note1>" + strLev1QuoteDetailCfg_Id + "</in_note1>";
				aml += "<in_note2>obselete_wbs_l2</in_note2>";
				aml += "<proj_num>" + Proj_Num + "</proj_num>";
				aml += "</Item></AML>";
				Item itmObsleteWBSL2 = inn.applyAML(aml);
				if (!itmObsleteWBSL2.isError())
				{
					//將作廢區ID列入排除的ID
					strLev2WBSIds += "," + itmObsleteWBSL2.getID();

					//將作廢區移到最後一個WBS之後                            
					sql = "Update [WBS_Element] set prev_item='" + ((prev_WBS_id[2] == null) ? "" : prev_WBS_id[2]) + "' where id='" + itmObsleteWBSL2.getID() + "'";
					inn.applySQL(sql);

				}

				aml = "<AML>";
				aml += "<Item type='Sub WBS' action='get'>";
				aml += "<source_id>" + itmThisRootWBS.getID() + "</source_id>";
				aml += "<related_id condition='not in'>'" + strLev2WBSIds.Replace(",", "','") + "'</related_id>";
				aml += "</Item></AML>";

				Item itmObsleteSubWBSs = inn.applyAML(aml);
				if (!itmObsleteSubWBSs.isError())
				{
					//砍掉作廢的WBS關聯
					aml = "<AML>";
					aml += "<Item type='Sub WBS' action='delete' where=\"[source_id]='" + itmThisRootWBS.getID() + "' and [related_id] not in('" + strLev2WBSIds.Replace(",", "','") + "')\" >";
					aml += "</Item></AML>";
					inn.applyAML(aml);

					//2.如果沒有「已作廢WBS」的WBS則創建一個                            
					if (itmObsleteWBSL2.isError())
					{
						itmObsleteWBSL2 = inn.newItem("WBS Element", "add");
						itmObsleteWBSL2.setProperty("name", "失效區");
						intLevel = 2;
						itmObsleteWBSL2.setProperty("prev_item", (prev_WBS_id[intLevel] == null) ? "" : prev_WBS_id[intLevel]);
						itmObsleteWBSL2.setProperty("proj_num", Proj_Num);
						itmObsleteWBSL2.setProperty("in_episode", z.ToString());
						itmObsleteWBSL2.setProperty("in_note1", strLev1QuoteDetailCfg_Id);
						itmObsleteWBSL2.setProperty("in_note2", "obselete_wbs_l2");
						//itmWBS.setProperty("in_quote_detail_cfgid", itmQuote_Detail.getProperty("in_config_id", ""));
						itmObsleteWBSL2 = itmObsleteWBSL2.apply();

						aml = "<AML>";
						aml += "<Item type='Sub WBS' action='add'>";
						aml += "<source_id>" + parent_id[intLevel - 1] + "</source_id>";
						aml += "<related_id>" + itmObsleteWBSL2.getID() + "</related_id>";
						aml += "</Item></AML>";
						itmSubWBS = inn.applyAML(aml);
					}

					aml = "<AML>";
					//aml += "<Item type='Sub WBS' action='get'>";
					//aml += "<source_id>" + itmObsleteWBSL2.getID() + "</source_id>";
					//aml += "<related_id>";
					aml += "<Item type='WBS Element' action='get'>";
					aml += "<in_episode>" + z.ToString() + "</in_episode>";
					aml += "<in_note1>" + strLev1QuoteDetailCfg_Id + "</in_note1>";
					aml += "<in_note2>obselete_wbs_l3</in_note2>";
					aml += "<proj_num>" + Proj_Num + "</proj_num>";
					//aml += "</Item>";
					//aml += "</related_id>";
					aml += "</Item></AML>";

					Item itmObsleteWBSL3 = inn.applyAML(aml);
					string strPrevObsleteWBSL3ID = "";
					if (itmObsleteWBSL3.isError())
					{
						itmObsleteWBSL3 = inn.newItem("WBS Element", "add");
						itmObsleteWBSL3.setProperty("name", "失效區");

						itmObsleteWBSL3.setProperty("prev_item", "");
						itmObsleteWBSL3.setProperty("proj_num", Proj_Num);
						itmObsleteWBSL3.setProperty("in_episode", z.ToString());
						itmObsleteWBSL3.setProperty("in_note1", strLev1QuoteDetailCfg_Id);
						itmObsleteWBSL3.setProperty("in_note2", "obselete_wbs_l3");
						//itmWBS.setProperty("in_quote_detail_cfgid", itmQuote_Detail.getProperty("in_config_id", ""));
						itmObsleteWBSL3 = itmObsleteWBSL3.apply();
						strPrevObsleteWBSL3ID = itmObsleteWBSL3.getID();

						aml = "<AML>";
						aml += "<Item type='Sub WBS' action='add'>";
						aml += "<source_id>" + itmObsleteWBSL2.getID() + "</source_id>";
						aml += "<related_id>" + itmObsleteWBSL3.getID() + "</related_id>";
						aml += "</Item></AML>";
						itmSubWBS = inn.applyAML(aml);
					}
					else
					{
						//找到最後一個WBS
						aml = "<AML>";
						aml += "<Item type='Sub WBS' action='get' select='related_id(id,prev_item,in_note1,name)'>";
						aml += "<source_id>" + itmObsleteWBSL2.getID() + "</source_id>";
						aml += "</Item></AML>";
						Item itmObsleteSubWBSL3s = inn.applyAML(aml);
						string strAllPreItems = "";
						for (int k = 0; k < itmObsleteSubWBSL3s.getItemCount(); k++)
						{
							itmObsleteWBSL3 = itmObsleteSubWBSL3s.getItemByIndex(k).getRelatedItem();
							strAllPreItems += itmObsleteWBSL3.getProperty("prev_item") + ",";
						}
						for (int k = 0; k < itmObsleteSubWBSL3s.getItemCount(); k++)
						{
							itmObsleteWBSL3 = itmObsleteSubWBSL3s.getItemByIndex(k).getRelatedItem();
							if (!strAllPreItems.Contains(itmObsleteWBSL3.getID()))
							{
								strPrevObsleteWBSL3ID = itmObsleteWBSL3.getID();
								break;
							}
						}

					}

					//3.將所有被刪除的WBS都移到已作廢WBS內
					//string strPrevItemId = itmObsleteWBSL3.getItemByIndex(itmObsleteWBSL3.getItemCount() - 1).getID();
					int intLastObsleteIndex = 0;
					for (int j = 0; j < itmObsleteSubWBSs.getItemCount(); j++)
					{
						Item itmObsleteSubWBS = itmObsleteSubWBSs.getItemByIndex(j);
						sql = "Update [WBS_Element] set prev_item='" + strPrevObsleteWBSL3ID + "',in_note1='" + (j + intLastObsleteIndex).ToString() + "' where id='" + itmObsleteSubWBS.getProperty("related_id") + "'";
						inn.applySQL(sql);


						aml = "<AML>";
						aml += "<Item type='Sub WBS' action='add'>";
						aml += "<source_id>" + itmObsleteWBSL2.getID() + "</source_id>";
						aml += "<related_id>" + itmObsleteSubWBS.getProperty("related_id") + "</related_id>";
						aml += "</Item></AML>";
						itmSubWBS = inn.applyAML(aml);


						strPrevObsleteWBSL3ID = itmObsleteSubWBS.getProperty("related_id");
					}
				}

			}




		}
	}
	catch (Exception ex)
	{
		string strError = (ex.InnerException == null ? ex.Message : ex.InnerException.Message);
		string strErrorDetail = "";
		strErrorDetail = strError + "\n" + ex.ToString();
		//Innosoft.InnUtility.AddLog(strErrorDetail, "Fatal", System.Reflection.MethodBase.GetCurrentMethod());
		throw new Exception(strError);
	}

	//要抓最後一個一階WBS，有可能是本次三集的最後一集，也可能是原本三集變成2集，但仍要提供第三集的WBS,以免後面的WBS會接錯階
	sql = "select id from [WBS_Element] where in_quote_detail_cfgid='" + strLev1QuoteDetailCfg_Id + "' and proj_num='" + Proj_Num + "' order by in_episode DESC";
	itmThisRootWBS = inn.applySQL(sql);
	if (itmThisRootWBS.isError())
	{
		itmThisRootWBS = null;
	}
	else
	{
		itmThisRootWBS = inn.getItemById("WBS Element", itmThisRootWBS.getItemByIndex(0).getID());
	}


	return itmThisRootWBS;
}

private Item MergeQuoteDetailToActivity2(Item itmQuote_Detail, Item itmAct2, string Act2Name, string prev_item, string proj_num, string episode, string wbsid, string strPredecessorAct2Id)
{
	Innovator inn=this.getInnovator();
	//itmAct2.setProperty("name", itmPart.getProperty("name",""));
	string strAction = itmAct2.getAction();
	if (itmAct2.getProperty("state", "") == "Complete")
		return itmAct2;

	if (strAction == "add")
	{
		itmAct2.setProperty("prev_item", prev_item);
		itmAct2.setProperty("expected_duration", "1");

	}

	itmAct2.setProperty("name", Act2Name);
	itmAct2.setProperty("proj_num", proj_num);
	itmAct2.setProperty("in_episode", episode);

	double dblQty = Convert.ToDouble(itmQuote_Detail.getProperty("in_qty", "0"));
	//工時
	double dblUnitWork = Convert.ToDouble(itmQuote_Detail.getProperty("in_part_unit_work", "0"));
	itmAct2.setProperty("work_est", Math.Round((dblUnitWork * dblQty), 0, MidpointRounding.AwayFromZero).ToString());

	//工期
	//double dblUnitDuration = Convert.ToDouble(itmQuote_Detail.getProperty("in_part_unit_duration", "0"));
	//double dblDuration = Math.Ceiling(dblUnitDuration * dblQty);
	//itmAct2.setProperty("expected_duration", dblDuration.ToString());

	//Leader Role
	if (itmQuote_Detail.getProperty("in_part_lead_role", "") != "")
		itmAct2.setProperty("lead_role", itmQuote_Detail.getProperty("in_part_lead_role", ""));
	else
	{
		//Leader
		if (itmQuote_Detail.getProperty("in_part_leader", "") != "")
			itmAct2.setProperty("managed_by_id", itmQuote_Detail.getProperty("in_part_leader", ""));
	}

	itmAct2.setProperty("in_quote_detail_cfgid", itmQuote_Detail.getProperty("in_config_id", ""));

	itmAct2.setProperty("in_note1", itmQuote_Detail.getProperty("in_note1", ""));
	itmAct2.setProperty("in_note2", itmQuote_Detail.getProperty("in_note2", ""));

	itmAct2 = itmAct2.apply();

	string strCurrentAct2Id = itmAct2.getID();
	if (strAction == "add")
	{
		Item itmWBSAct2 = inn.newItem("WBS Activity2", "add");
		itmWBSAct2.setProperty("source_id", wbsid);
		itmWBSAct2.setProperty("related_id", itmAct2.getID());
		itmWBSAct2 = itmWBSAct2.apply();

		//建立前置任務
		if (strPredecessorAct2Id != "")
		{
			Item itmPredecessor = inn.newItem("Predecessor", "add");
			itmPredecessor.setAttribute("serverEvents", "0");
			itmPredecessor.setProperty("precedence_type", "Start to Start");
			itmPredecessor.setProperty("source_id", itmAct2.getID());
			itmPredecessor.setProperty("related_id", strPredecessorAct2Id);
			itmPredecessor = itmPredecessor.apply();
		}
	}
	return itmAct2;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_QuoteToProject' and [Method].is_current='1'">
<config_id>07548BB668BA4951A3941F2BD8BBDBCA</config_id>
<name>In_QuoteToProject</name>
<comments>報價轉專案</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
