Dim z As item = Me.newItem("SQL", "SQL PROCESS")
z.setProperty("name", "PE_RollupAllPartsInDB")
z.setProperty("PROCESS", "CALL")
z.setProperty("TYPE", "Part")
Dim result As Item = z.apply()
If result.isError() Then return result
Return Me

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_RollupAllPartsInDB' and [Method].is_current='1'">
<config_id>EDFC2A5512A144899955E70BC383FCB7</config_id>
<name>PE_RollupAllPartsInDB</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
