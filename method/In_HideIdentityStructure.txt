//debugger;
 setTimeout("top.inHideIndentityTab();", 100);

top.inHideIndentityTab = function()
{
  if (!parent.relationships)
  {
    setTimeout("top.inHideIndentityTab();", 100);
    return; 
  }
   if (parent.relationships.document.readyState != "complete" || !parent.relationships.currTabID)
   {
    setTimeout("top.inHideIndentityTab();", 100); 
   }
  else
  {
	  if (parent.thisItem.getProperty("in_is_org") == "0" || parent.thisItem.getProperty("in_is_org") === null) {
    var tabbar = parent.relationships.relTabbar;
	var tabID =  top.aras.getRelationshipTypeId("Identity Structure");
	tabbar.SetTabVisible(tabID,false);
	  }
  }
} ;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_HideIdentityStructure' and [Method].is_current='1'">
<config_id>F8DE5BD3CBB14077972F28709934B138</config_id>
<name>In_HideIdentityStructure</name>
<comments>Add by Inno</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
