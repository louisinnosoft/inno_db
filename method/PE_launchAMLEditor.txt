var relatedPart = parent.item.selectSingleNode("Relationships/Item[@id='" + inDom.getAttribute("id") + "']/related_id/Item");
if (!relatedPart)
{
	top.aras.AlertError(top.aras.getResource("plm", "launch_aml_editor.related_part_is_empty"));
	return;
}

var query = new Item("Part", "get");
query.setID(relatedPart.getAttribute("id"));
query = query.apply();

if (query.getItemCount() != 1)
{
	if (query.isError())
		top.aras.AlertError(query.getErrorDetail(), query.getErrorString(), query.getErrorSource());
	else
		top.aras.AlertError(top.aras.getResource("plm", "launch_aml_editor.no_parts_found"));
	return;
}

var param = top.aras.newObject();
param.title = "AML Editor for Part " + top.aras.getItemProperty(relatedPart, "keyed_name");
param.formId = "5FFC8F61CCFD4D1893C4DF7987720C8B"; //Part AML Editor Form
param.aras = top.aras;
param.item = query;
var options = {
	dialogWidth: 800,
	dialogHeight: 385,
	resizable: 'no'
};
var res = top.aras.modalDialogHelper.show('DefaultPopup', window, param, options, 'ShowFormAsADialog.html');
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_launchAMLEditor' and [Method].is_current='1'">
<config_id>00C75D5044234E8DA8071BDB3132F0F4</config_id>
<name>PE_launchAMLEditor</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
