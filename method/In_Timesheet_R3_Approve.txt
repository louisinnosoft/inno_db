/*
目的:核准(in_verify)工時紀錄表(In_TimeRecord)並刪除核准的關聯:工時審核-今天(In_Timesheet_R1),工時審核-昨天(In_Timesheet_R2),工時審核-前天(In_Timesheet_R3)
做法:
1.檢查表單是否上鎖
2.使用In_SendEmail_RejectTimesheet
3.顯示提醒
4.顯示審核筆數
位置:物件動作
*/

var inn = this.getInnovator();

//1.檢查表單是否上鎖
if(this.getLockStatus() !== 0)
{
	alert(inn.applyMethod("In_Translate","<text>請先解鎖本文件再執行</text>").getResult());
	return;
}

//2.使用In_SendEmail_RejectTimesheet
var itmApplyMethod = inn.applyMethod("In_SendEmail_RejectTimesheet","<ItemType>In_Timesheet_R3</ItemType><SourceID>" + this.getProperty("id","") + "</SourceID>");

//3.顯示提醒
if(itmApplyMethod.getProperty("ErrMsg","0") !== ""){
    var strMsg = "三天前工時審核表:\n"+itmApplyMethod.getProperty("ErrMsg","0");
    alert(inn.applyMethod("In_Translate","<text>" + strMsg + "</text>").getResult());
}

//4.顯示審核筆數
var strMsg = "三天前工時審核表:\n總共" + itmApplyMethod.getProperty("all","0") + "筆,\n核准" + itmApplyMethod.getProperty("approved","0") + "筆,\n核退" + itmApplyMethod.getProperty("reject","0") + "筆";
alert(inn.applyMethod("In_Translate","<text>" + strMsg + "</text>").getResult());

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Timesheet_R3_Approve' and [Method].is_current='1'">
<config_id>1DD24A6D846C4EFFAEC2477015F48AAC</config_id>
<name>In_Timesheet_R3_Approve</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
