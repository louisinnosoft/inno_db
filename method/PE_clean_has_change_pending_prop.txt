string type = this.getType();
if (String.Equals(type, "Part", StringComparison.Ordinal))
{
	// Reorders the relationships to put all Part Goal items at the end
	Item goals = this.getRelationships("Part Goal");
	for (int i = 0; i < goals.getItemCount() - 1; i++)
	{
		Item goal = goals.getItemByIndex(i);
		this.removeRelationship(goal);
		this.addRelationship(goal);
	}
}

this.removeProperty("has_change_pending");
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_clean_has_change_pending_prop' and [Method].is_current='1'">
<config_id>F1B7B3EDC8C348A7AD0382D55A9AD99A</config_id>
<name>PE_clean_has_change_pending_prop</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
