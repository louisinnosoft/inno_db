var docEditor = window.cmfContainer.qpWindow.documentEditor;
if (docEditor) {
	docEditor.undo();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_ToolbarUndoHandler' and [Method].is_current='1'">
<config_id>E5A5FDB95B2947BE9B51A92D1C06B109</config_id>
<name>cmf_ToolbarUndoHandler</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
