/*
目的:產生一組GUID,填入報價細項的 in_config_id,確保無論source item如何改版,都可以找到同一個報價項的脈絡
位置:In_Quote_Details onBeforeAdd
做法:
1.產生GUID
2.塞進去 in_config_id
*/

Innovator inn = this.getInnovator();
string newId = inn.getNewID();
this.setProperty("in_config_id",newId);
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GenerateQuoteDetailConfigId' and [Method].is_current='1'">
<config_id>28196DC36B4F4BC6BEFFFF575C90C9C0</config_id>
<name>In_GenerateQuoteDetailConfigId</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
