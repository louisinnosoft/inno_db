var topWindow = aras.getMostTopWindowWithAras(window);
var menuFrame = topWindow.menu;
if (menuFrame && menuFrame.onOffDStateCommand) {
	localStorage.setItem('defaultDState', 'offDState');
	menuFrame.onOffDStateCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_onOffDState' and [Method].is_current='1'">
<config_id>C78370D703FA42D49B499143FE42D8C2</config_id>
<name>cui_default_mwmm_onOffDState</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
