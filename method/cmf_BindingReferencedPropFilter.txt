var filter = {};
var referenceType = aras.getItemProperty(inArgs.itemContext, 'reference_type');
filter['source_id'] = {
	filterValue: referenceType ? aras.MetadataCache.GetItemTypeName(referenceType) : null,
	isFilterFixed: true
};
return filter;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_BindingReferencedPropFilter' and [Method].is_current='1'">
<config_id>1D8406D5ACCD4F0681B59ADB26225BFE</config_id>
<name>cmf_BindingReferencedPropFilter</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
