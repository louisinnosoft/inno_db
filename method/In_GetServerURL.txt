
''目的:在 Server Side 取得 URL
''位置:可以在Email Message 的 Query Content 內使用
''做法:
''參考 sendEmailToA2Assignee Method

'System.Diagnostics.Debugger.Break()
Dim host As String = HttpContext.Current.Request.Url.Host
Dim port As String = HttpContext.Current.Request.Url.Port
Dim vDir As String = ""
Dim protocol As String="http"
Dim count As Integer = HttpContext.Current.Request.Url.Segments.Count
For index As Integer = 1 To (count - 3) 'Ignoring "Server/InnovatorServer.aspx"
	vDir +=  HttpContext.Current.Request.Url.Segments(index)
Next
If port <> "80" Then 
	host +=  ":" + port
End If

If HttpContext.Current.Request.IsSecureConnection <> False Then
	protocol = "https"
End If


Dim ServerURL As String = protocol & "://" & host & "/" & vDir

'常用的網址應該是: protocol & "://" & host & "/" & vDir & "default.aspx?StartItem=Activity2:

Me.setProperty("In_Value",ServerURL)

Return Me


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetServerURL' and [Method].is_current='1'">
<config_id>AC2390ABDC5240938AC93A612FDF2616</config_id>
<name>In_GetServerURL</name>
<comments>Inn Core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
