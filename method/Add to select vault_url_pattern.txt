

			string selectAttributeValue = this.getAttribute("select");
			if (string.IsNullOrEmpty(selectAttributeValue))
			{
				return this;
			}

			bool isVaultUrlInSelect = false;
			string[] values = selectAttributeValue.Split(',');

			foreach (string attributeToSelect in values)
			{
				switch (attributeToSelect.Trim())
				{
					case "vault_url_pattern":
						return this;
					case "vault_url":
						isVaultUrlInSelect = true;
						break;
				}
			}

			if (!isVaultUrlInSelect)
			{
				return this;
			}

			selectAttributeValue += ", vault_url_pattern";
			this.setAttribute("select", selectAttributeValue);

			return this;

			
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Add to select vault_url_pattern' and [Method].is_current='1'">
<config_id>151D45AE6E054D26B300673092DE7C8A</config_id>
<name>Add to select vault_url_pattern</name>
<comments>Add to select vault_url_pattern for set vault_url federated prop (IR-014890)</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
