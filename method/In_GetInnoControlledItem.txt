/*
GetInnoControlledItem
目的:接收Vote.aspx傳過來的需求,要回應最新的簽審資料資訊
*/

//System.Diagnostics.Debugger.Break();


Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
Item itmR = null;	
try
{
	string strActId = this.getProperty("actid","");
	Item itmAct = inn.getItemById("Activity",strActId);
	itmR = _InnH.GetInnoControlledItem(itmAct);
	if(itmR.isError())
	{
	    throw new Exception("無法取得簽核文件,可能是無權限");
	}
}
catch(Exception ex)
{	


	string strError = (ex.InnerException==null?ex.Message:ex.InnerException.Message);
	string strErrorDetail="";
	if(strError=="Exception of type 'Aras.Server.Core.InnovatorServerException' was thrown.")
	{
		strError = "無法執行AML:" + aml ;
	}
	strErrorDetail = strError + "\n" + ex.ToString() + aml  + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	return inn.newError(_InnH.Translate(strError));
}


return itmR;
		
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetInnoControlledItem' and [Method].is_current='1'">
<config_id>C1FEE296CD224C198EBB7AE959BE4ACF</config_id>
<name>In_GetInnoControlledItem</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
