var doclibNameList = this;
var id = '';
var name = '';
if (doclibNameList.selectedIndex > -1) {
	id = doclibNameList.options[doclibNameList.selectedIndex].value;
	name = doclibNameList.options[doclibNameList.selectedIndex].text;
}
top.aras.setItemProperty(document.item, 'sp_doclib_name', name);
handleItemChange('sp_doclib_id', id);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SPDocLibDef_onNameChange' and [Method].is_current='1'">
<config_id>3F0DE2FFF1B94238B810EAA8019BE8C7</config_id>
<name>SPDocLibDef_onNameChange</name>
<comments>Handles changes of doclib name to set corresponding value in doclib id field</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
