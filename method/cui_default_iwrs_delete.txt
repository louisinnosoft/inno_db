if (this.computeCorrectControlState) {
	if (state.contentEditable) {
		return true;
	}

	var isDeleteEnabled = this.computeCorrectControlState('delete');
	if (isDeleteEnabled) {
		var msg = aras.getResource('', 'relationshipsgrid.delete_confirmation');
		if (!aras.confirm(msg)) {
			return;
		}
		this.processCommand('delete');
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_iwrs_delete' and [Method].is_current='1'">
<config_id>6B91D4B59C3F45F091216ACB27FD3714</config_id>
<name>cui_default_iwrs_delete</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
