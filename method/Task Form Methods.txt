var topWnd = aras.getMostTopWindowWithAras(window);
window['TaskForm_HideMenu'] = function() {
	if (topWnd.item.getAttribute('type') == 'Form') {
		return;
	}
	var style = document.styleSheets[0];
	var parentDoc = topWnd.document;
	var menuContainer = parentDoc.getElementById('top');

	style.insertRule('html, body {width: 100%; height: 100%; overflow: hidden;}', 0);

	if (menuContainer) {
		var formPaneWidget = parentDoc.getElementById('formContentPane');
		var menuHeight = menuContainer.offsetHeight;

		menuContainer.style.display = 'none';
		style = parentDoc.styleSheets[parentDoc.styleSheets.length - 1];
		style.insertRule('#center {top: 0px !important;}', style.cssRules.length);
		formPaneWidget.style.height = parseInt(formPaneWidget.style.height) + menuHeight + 'px';
	}
};

window['TaskForm_CreateDialogFrame'] = function(dialogUrl, onloadHandler) {
	if (topWnd.item.getAttribute('type') == 'Form') {
		return;
	}
	var iframe = document.createElement('iframe');
	iframe = document.body.appendChild(iframe);
	var st = iframe.style;
	st.width = st.height = '100%';
	iframe.setAttribute('border', 'no');
	if (onloadHandler) {
		iframe.onload = onloadHandler;
	}
	iframe.setAttribute('src', dialogUrl);
	return iframe;
};

window['TaskForm_InvalidateItemsGridRow'] = function() {
	var itm = document.item;
	var id = itm.getAttribute('id');
	aras.itemsCache.deleteItem(id);
	var newItm = aras.getItemById(itm.getAttribute('type'), id);
	if (newItm) {
		topWnd.updateItemsGrid(newItm);
	} else {
		topWnd.deleteRowFromItemsGrid(id);
	}
	aras.itemsCache.deleteItem(id);
};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Task Form Methods' and [Method].is_current='1'">
<config_id>A003399F6405496B89A9E3F8EF67BB18</config_id>
<name>Task Form Methods</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
