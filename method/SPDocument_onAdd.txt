//MethodTemplateName=CSharpInOut;
throw new Exception( "Can't add SharePoint document from Innovator" );
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SPDocument_onAdd' and [Method].is_current='1'">
<config_id>242393D77B3044AE919DA7961822CB43</config_id>
<name>SPDocument_onAdd</name>
<comments>Returns an error on attemp to add document to SP from Innovator</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
