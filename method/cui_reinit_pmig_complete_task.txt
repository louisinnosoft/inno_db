var disabled = true;
if (inArgs.isReinit) {
	if (!inArgs.eventState.InBasketEventState) {
		var states = aras.evalMethod('cui_reinit_inbasket_calc_states', '', {eventType: inArgs.eventType});
		inArgs.eventState.InBasketEventState = states.InBasketEventState;
	}

	if (inArgs.eventState.InBasketEventState) {
		var inBasketEventState = inArgs.eventState.InBasketEventState;
		if (inBasketEventState.itemIds.length !== 1) {
			return {'cui_disabled': true, 'cui_invisible': true};
		}

		disabled = inBasketEventState.discoverOnlyFlg && isFunctionDisabled(inBasketEventState.itemTypesNames[0], 'Edit');
	}
}

return {'cui_disabled': disabled, 'cui_invisible': disabled};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_pmig_complete_task' and [Method].is_current='1'">
<config_id>0C0987174E9A4DD5BF30EF06D53C2C23</config_id>
<name>cui_reinit_pmig_complete_task</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
