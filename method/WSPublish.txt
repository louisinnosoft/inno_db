function IsValid(_name) {
	var fvar = '<WSConfiguration action=\'verify\' name=\'' + _name + '\' />';
	var xmlResp = aras.applyMethod('VerifyWSConfiguration', fvar);

	var d = aras.createXMLDocument();
	d.loadXML(xmlResp);
	var nd = d.selectSingleNode('./Result/type');
	return (nd === null);
}

function IsValidServiceName(_sName) {
	var expression = '^[a-zA-Z_]{1}[a-zA-Z0-9_]+$';
	if (_sName.match(expression) !== null) {
		return true;
	} else {
		return false;
	}
}

var d = aras.createXMLDocument();
d.loadXML(inDom.xml);
var nd = d.selectSingleNode('.//Item[@type=\'WSConfiguration\']/name');
if (nd === null) {
	aras.AlertError(aras.getResource('', 'imports_core.ws_save_configuration_xml__please_save'));
	return null;
}

// is changes saved?
var isDirty = d.selectSingleNode('.//Item[@type=\'WSConfiguration\' and @isDirty]');
if (isDirty !== null) {
	aras.AlertError(aras.getResource('', 'imports_core.ws_save_configuration_xml__please_save'));
	return null;
}

var name = nd.text;
if (!IsValid(name)) {
	aras.AlertError(aras.getResource('', 'imports_core.ws_configuration_is_invalid'));
	return null;
}

return aras.prompt(aras.getResource('', 'imports_core.ws_enter_service_name'), name, window).then(function(serviceName) {
	if (!serviceName) {
		return null;
	}

	if (!IsValidServiceName(serviceName)) {
		aras.AlertError(aras.getResource('', 'imports_core.ws_service_name_is_invalid'));
		return null;
	}

	var fvar = '<WSConfiguration action=\'publish\' name=\'' + name + '\' service_name=\'' + serviceName + '\' />';
	var xmlConfItem = aras.applyMethod('PublishWebService', fvar);
	if (xmlConfItem) {
		aras.AlertSuccess(aras.getResource('', 'imports_core.ws_successfully_published'));
	}
	return xmlConfItem;
});

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='WSPublish' and [Method].is_current='1'">
<config_id>3F35EF6F3C0B4BE084C1287A3832AB8D</config_id>
<name>WSPublish</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
