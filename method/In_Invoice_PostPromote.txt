/*
目的:銷貨單Promote的時候,要執行事項
位置:In_invoice Lifecycle PostMethod
說明:

*/
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
string aml = "";
string sql = "";
string error= "";
Item itmInvoiceDetails;
Item itmInvoiceDetail;
Item itmR;
string state = this.getProperty("state","");

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);


try
{
	switch(state)
	{
		case "Start":		
			break;
		case "In Review":
			break;
		case "In Confirm":
			break;
		case "Done":
			Item itmQuote = inn.getItemById("In_Quote",this.getProperty("in_ref_quote",""));
			itmQuote.apply("In_SumInvoiceToQuote");
			break;
		case "Cancel":
			break;
		default:
			break;
			
	}

	if(error!="")
		throw new Exception(error);
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	return inn.newError(ex.InnerException==null?ex.Message:ex.InnerException.Message);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
return this;	
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Invoice_PostPromote' and [Method].is_current='1'">
<config_id>1EA3A4BC5ED0407299EA7ADB7C69180F</config_id>
<name>In_Invoice_PostPromote</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
