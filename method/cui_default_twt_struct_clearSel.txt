var control = inArgs.control;
if (control && control['_item_Experimental'] && control['_item_Experimental'].ownerDocument) {
	var context = control['_item_Experimental'].ownerDocument.defaultView;
	if (context && context.onSetNothingCommand) {
		return context.onSetNothingCommand();
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_twt_struct_clearSel' and [Method].is_current='1'">
<config_id>9CDE2A6EC0A0468E99D0A016F7272EA5</config_id>
<name>cui_default_twt_struct_clearSel</name>
<comments>Structure Toolbar Clear Selected Button</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
