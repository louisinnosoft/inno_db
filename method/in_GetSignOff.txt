//System.Diagnostics.Debugger.Break();
//in_GetSignOff
Innovator inn = this.getInnovator();
string criteria = this.getProperty("criteria");
criteria = criteria.Replace("ProcessId=","");
string UserInfo = this.getProperty("userinfo","");
string r = "";
	Innosoft.app _InnoApp = new Innosoft.app(inn,UserInfo);
try
{
	r = _InnoApp.GetSignOff(criteria);
}
catch (Exception ex)
{
	criteria = "<request><![CDATA[" + criteria + "]]></request>";
	r = _InnoApp.BuildResponse("false",ex.Message,"",criteria);
}


return  inn.newResult(r);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='in_GetSignOff' and [Method].is_current='1'">
<config_id>11563FD5056849A98560297D1C58077E</config_id>
<name>in_GetSignOff</name>
<comments>Add By Inno</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
