//目的:重啟排成佇列
//做法:將In_Queue的In_State欄位強制更新為close

Innovator inn = this.getInnovator();
this.setProperty("in_state","close");
this.apply("edit");

return this;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Queue_Close' and [Method].is_current='1'">
<config_id>3C2CB51A47844AE2B4A00444342A306A</config_id>
<name>In_Queue_Close</name>
<comments>Queue</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
