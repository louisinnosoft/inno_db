//System.Diagnostics.Debugger.Break();
/*目的:藥證文件轉換完成後的後處理
Context Item : In_Queue
作業標的:In_Drug_Lic_Deliverable
位置:藥證轉檔佇列的 In_queue 的 in_state_method 欄位內
做法:
1.In_Queue 的狀態變成 To Closed : 將 In_Drug_Lic_Deliverable 的 in_convert_type 與 in_queue_state 清空
*/


Innovator inn = this.getInnovator();
string sql;
string aml;
Item itmSQL = null;

//2.依據狀態變更 in_start_time 與 in_end_time
sql = "";
switch(this.getProperty("state",""))
{
	case "Start":		
		break;
	case "In_Process":
		break;
	case "Pendding":
		break;
	case "Failed":
		break;
	case "Closed":
		sql = "Update In_Drug_Lic_Deliverable set in_convert_type='',in_queue_state='' where id='" + this.getProperty("in_source_id","") + "'";
		break;
	default:
		break;
	}

if(sql!="")
	itmSQL = inn.applySQL(sql);

return itmSQL;



#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Queue_DLDel' and [Method].is_current='1'">
<config_id>52C1E696E22F49D0B303727C67A13874</config_id>
<name>In_Queue_DLDel</name>
<comments>drug</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
