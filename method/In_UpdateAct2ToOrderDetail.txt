//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
	
try
{
/*	此行會複製審核者
string strCopyProperties = "name,in_name;date_due_sched,in_act2_due_sched;modified_on,in_act2_modified_on;date_start_sched,in_act2_start_sched;state,in_act2_state;managed_by_id,in_manager;in_proj_name,in_proj_name;in_proj_state,in_proj_state;in_executor,in_new_assignment";
*/	
	string strCopyProperties = "name,in_name;date_due_sched,in_act2_due_sched;modified_on,in_act2_modified_on;date_start_sched,in_act2_start_sched;state,in_act2_state;in_proj_name,in_proj_name;in_proj_state,in_proj_state;in_executor,in_new_assignment";
	
aml = "<AML>";
	aml += "<Item type='In_WorkOrder_Detail' action='get' select='id,related_id'>";
	aml += "<source_id>" + this.getID() + "</source_id>";
	aml += "</Item></AML>";
	
	Item itmWorkOrder_Details = inn.applyAML(aml);
	for(int i=0;i<itmWorkOrder_Details.getItemCount();i++)
	{
		Item itmWorkOrder_Detail = itmWorkOrder_Details.getItemByIndex(i);
		string getRelatedID = itmWorkOrder_Detail.getProperty("related_id","");
		if(getRelatedID!=""){
			_InnH.CopyProperties("Activity2",itmWorkOrder_Detail.getProperty("related_id",""),"In_WorkOrder_Detail",itmWorkOrder_Detail.getID(),strCopyProperties,"0",false,false);
		}
	}
	

	
	sql = "Update [In_WorkOrder_Detail] set ";
	sql += "in_event_description=''";
	sql += ",in_event_enddate=in_act2_due_sched";
	sql += ",in_event_location=''";
	sql += ",in_event_startdate=in_act2_start_sched";
	sql += ",in_event_title=in_name";
	sql += ",in_event_who=in_new_assignment";
	sql += ",in_event_type='派工單'";
	sql += " where source_id='" + this.getID() + "'";
	inn.applySQL(sql);

}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	throw new Exception(ex.InnerException==null?ex.Message:ex.InnerException.Message);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateAct2ToOrderDetail' and [Method].is_current='1'">
<config_id>B6B944FCC1FA47B5AEFC923366CA4F43</config_id>
<name>In_UpdateAct2ToOrderDetail</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
