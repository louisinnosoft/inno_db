/*
位置:獨立存在,透過schedule處理
目的:專案表頭新增總工時欄位，並將所有專案任務之工時累加至表頭總工時欄位中
做法:
1.找出active的project id
2.使用SQL語法,select sum(xxxx) as c1 from 工時紀錄表 where projectid in(xx,xx,xx,xx) group by projectid
3.再依序使用sql update語法將各專案計算的總工時更新到正確欄位
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string sql = "";
string strProjID = "";
Item itmSQL = null;

sql = "SELECT[ID]FROM[PROJECT]WHERE[STATE]='ACTIVE'";
itmSQL = inn.applySQL(sql);

for(int i=0;i<itmSQL.getItemCount();i++){
	Item itmSQLs = itmSQL.getItemByIndex(i);
	strProjID+="'"+itmSQLs.getProperty("id","")+"'"+",";
}
strProjID = strProjID.Trim(',');

sql = "SELECT[IN_PROJECT],SUM(IN_TODAY_HOURS)AS[TOTALTIME]FROM[IN_TIMERECORD]WHERE[IN_PROJECT]IN("+strProjID+")GROUP BY[IN_PROJECT]";

itmSQL = inn.applySQL(sql);
for(int i=0;i<itmSQL.getItemCount();i++){
	Item itmSQLs = itmSQL.getItemByIndex(i);
	
	sql = "UPDATE[PROJECT]SET[IN_HOUR_SUM_ACT]='"+itmSQLs.getProperty("totaltime","")+"' WHERE[ID]='"+itmSQLs.getProperty("in_project","")+"'";
inn.applySQL(sql);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SumAllWorkHoursToProject' and [Method].is_current='1'">
<config_id>570B2508FBA04104BE2863BD1FE72E54</config_id>
<name>In_SumAllWorkHoursToProject</name>
<comments>專案表頭新增總工時欄位，並將所有專案任務之工時累加至表頭總工時欄位中</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
