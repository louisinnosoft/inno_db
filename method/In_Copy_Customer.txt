/*
目的:將Customer(客戶)的欄位帶到In_Invoice(客戶請款單)的欄位
使用In_CopyProsValueFromProItem
*/

var FromPropertyName = "in_customer";
var PropertyMappings = "in_tax_code,in_tax_code;";
document.In_CopyProsValueFromProItem(FromPropertyName,PropertyMappings,"Y");
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Copy_Customer' and [Method].is_current='1'">
<config_id>83CB7CDE44A4428F9D9D6967048D13D7</config_id>
<name>In_Copy_Customer</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
