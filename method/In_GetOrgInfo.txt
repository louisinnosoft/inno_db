/*
目的:取得部門資訊(1,2階主管部門)
做法:
傳入參數:<identity_id>Identity</identity_id><info_type>manager1:一階主管,manager2:二階主管,my_org:所屬部門,my_org2:二階部門</info_type>
位置:
*/
string strMethodName = "In_GetOrgInfo";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
_InnH.AddLog(strMethodName,"MethodSteps");
string aml = "";
string sql = "";
string str_r = "";
string identity_id = this.getProperty("identity_id","");
string info_type = this.getProperty("info_type","");

Item itmR = this;
	
try
{
	string strIdentityId = _InnH.GetOrgInfo(identity_id,info_type);
	itmR = inn.newResult(strIdentityId);
	
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";
		
	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	_InnH.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetOrgInfo' and [Method].is_current='1'">
<config_id>C1DA0495387C4BCA8BDBCD859E92CB51</config_id>
<name>In_GetOrgInfo</name>
<comments>取得部門資訊(1,2階主管部門)</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
