//System.Diagnostics.Debugger.Break();
/*
目的:判斷原始檔案(in_native_file)若有變更要發信通知owner
位置:onAfterUpdate
做法:
1.取得前一generation的in_native_file值
2.比對本generation與前generation的in_native_file值,若不相等則發出email通知
*/

Innovator inn = this.getInnovator();
string aml = "";
aml = "<AML>";
aml += "<Item type='" + this.getType() + "' action='get' select='in_native_file,description'>";
aml += "<config_id>" + this.getProperty("config_id") + "</config_id>";
aml += "<generation>" + (Convert.ToInt32(this.getProperty("generation"))-1).ToString() + "</generation>";
aml += "</Item></AML>";
Item itmPre = inn.applyAML(aml);
if(itmPre.isError())
	return this;

if(itmPre.getProperty("in_native_file","")!=this.getProperty("in_native_file",""))
{
	return this;
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_NativeFileChanged' and [Method].is_current='1'">
<config_id>64C3299E4324413EBF930A7A067FA0E2</config_id>
<name>In_NativeFileChanged</name>
<comments>inn drug</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
