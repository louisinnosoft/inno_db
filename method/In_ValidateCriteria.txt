//System.Diagnostics.Debugger.Break();
/*
目的:檢查傳入的物件是否符合傳入的條件
傳入參數:<itemtype>Document<itemtype><item_id>8393820982</item_id><criteria_id>asdadasdas</criteria_id>
做法:
1.將傳入的物件與Criteria的條件是組成 sql 語法
2.如果傳回筆數為1,則代表判斷是成立,如果為0,則代表傳入的物件不符合條件
*/

Innovator inn = this.getInnovator();
string strItemtype = this.getProperty("itemtype","");
string strItem_Id = this.getProperty("item_id","");
string criteria_id = this.getProperty("criteria_id","");
Item itmCriteria = inn.getItemById("In_Criteria",criteria_id);
string strRule = itmCriteria.getProperty("in_rule","");

string sql = "";
sql = "select count(*) as c from [" + strItemtype.Replace(" ","_") + "] where id='" + strItem_Id + "'";
if(strRule!="")
	sql += " and (" + strRule + ")";

Item itmResult = inn.applySQL(sql);

if(itmResult.getItemCount()==0)
	return inn.newResult("false");

if(itmResult.getItemCount()!=0)
	return inn.newResult("true");

return this;


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ValidateCriteria' and [Method].is_current='1'">
<config_id>B08D8BC3D75F458A81691605DD327D7B</config_id>
<name>In_ValidateCriteria</name>
<comments>inn core criteria</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
