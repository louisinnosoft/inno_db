var control = inArgs.control;
if (control && control['_item_Experimental'] && control['_item_Experimental'].ownerDocument) {
	var context = control['_item_Experimental'].ownerDocument.defaultView;
	if (context && context.onUndoCommand) {
		control.setEnabled(false);
		return context.onUndoCommand();
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_twt_structure_undo' and [Method].is_current='1'">
<config_id>776A7931B7F4499490A1BF8F23D2B9CA</config_id>
<name>cui_default_twt_structure_undo</name>
<comments>Structure Toolbar Undo Button</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
