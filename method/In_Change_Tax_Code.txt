/*
目的:將稅別碼更換與加上符號
做法:
1.取得稅別碼
2.更新稅別碼
3.更新欄位
*/

//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();

string aml = "";
string sql = "";

//1.取得稅別碼
aml = "<AML>";
aml += "<Item type='In_tax_code' action='get'>";
aml += "</Item></AML>";
Item itmTaxCodes = inn.applyAML(aml);
for(int i=0;i<itmTaxCodes.getItemCount();i++){
	Item itmTaxCode = itmTaxCodes.getItemByIndex(i);
	
	//2.更新稅別碼
	string strTaxCode = "," + itmTaxCode.getProperty("in_company_code","") + ",";
	strTaxCode = strTaxCode.Replace("、",",");

//3.更新欄位
sql = "Update [IN_Tax_Code] set ";
sql += "[in_company_code] = '" + strTaxCode + "'";
sql += " where id = '" + itmTaxCode.getID() + "'";
inn.applySQL(sql);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Change_Tax_Code' and [Method].is_current='1'">
<config_id>CC90A12527C24484A203EC1DCDA1DA52</config_id>
<name>In_Change_Tax_Code</name>
<comments>只能執行一次</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
