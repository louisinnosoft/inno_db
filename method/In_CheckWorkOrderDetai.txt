/*
目的:確認派工單身的必填欄位沒有錯誤,如有錯誤會顯示錯誤訊息,如無錯誤會將開始與結束日期寫入表頭與流程
1.開始日期應小於結束日期
2.必填欄位:任務預計開始日期,任務預計結束日期,任務執行者,任務審核人員,專案名稱
3.將表身的最小開始日期與最大結束日期寫入表頭與流程
*/

// //System.Diagnostics.Debugger.Break();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string chk = "";
string strErr = "";
string strNull = "";

chk = "<AML>";
chk += "<Item type='In_WorkOrder_Detail' action='get'>";
chk += "<source_id>" + this.getID() + "</source_id>";
chk += "</Item></AML>";
Item itmCheck = inn.applyAML(chk);

for(int i=0;i<itmCheck.getItemCount();i++)
{
    string strTempErr = "";
    Item itmChecks = itmCheck.getItemByIndex(i);

    if(itmChecks.getProperty("in_act2_due_sched","")!="")
    {
        string strDateStart = itmChecks.getProperty("in_act2_start_sched","");
        string strDateEnd = itmChecks.getProperty("in_act2_due_sched","");
        DateTime dateTime1 = Convert.ToDateTime(strDateStart);
        DateTime dateTime2 = Convert.ToDateTime(strDateEnd);

        if(dateTime1.CompareTo(dateTime2) > 0) 
        {
            strTempErr += "[開始日期應小於結束日期]";
        }
    }
    if(itmChecks.getProperty("in_act2_start_sched","") == "")
    {
        strTempErr += "[任務預計開始日期]";
    }
    if(itmChecks.getProperty("in_act2_due_sched","") == "")
    {
        strTempErr += "[任務預計結束日期]";
    }
    if(itmChecks.getProperty("in_new_assignment","") == "")
    {
        strTempErr += "[執行者]";
    }
    if(itmChecks.getProperty("in_name","") == "")
    {
        strTempErr += "[名稱]";
    }
    if(strTempErr !="")
    {
        strErr += "派工細項#"+itmChecks.getProperty("sort_order","") +":"+strTempErr+"<br>";
    }
}

if(strErr!="")
{
    strNull += "以下欄位必填:" +"<br>"+ strErr;
    return inn.newError(_InnH.Translate(strNull));
}

string sql = "";
string strMinDate = "";
string strMaxDate = "";

aml  = "<AML>";
aml += "<Item type='In_WorkOrder_Detail' action='get' select='in_act2_start_sched,in_act2_due_sched'>";
aml += "<source_id>"+this.getID()+"</source_id>";
aml += "</Item></AML>";
Item itmWorkOrderDetails = inn.applyAML(aml);

for(int i=0;i<itmWorkOrderDetails.getItemCount();i++)
{
    Item itmWorkOrderDetail = itmWorkOrderDetails.getItemByIndex(i);
    DateTime dtMinDate = Convert.ToDateTime(itmWorkOrderDetail.getProperty("in_act2_start_sched").Split('T')[0]);
    DateTime dtMaxDate = Convert.ToDateTime(itmWorkOrderDetail.getProperty("in_act2_due_sched").Split('T')[0]);

    if(strMinDate=="")
    {
        strMinDate = dtMinDate.ToString("yyyy-MM-dd");
    }
    else
    {
        DateTime dtDate = Convert.ToDateTime(itmWorkOrderDetail.getProperty("in_act2_start_sched").Split('T')[0]);
        dtMinDate = Convert.ToDateTime(strMinDate);

        if(DateTime.Compare(dtDate,dtMinDate)<0)
        {
            strMinDate = dtDate.ToString("yyyy-MM-dd");
        }
    }

    if(strMaxDate=="")
    {
        strMaxDate = dtMaxDate.ToString("yyyy-MM-dd");
    }
    else
    {
        DateTime dtDate = Convert.ToDateTime(itmWorkOrderDetail.getProperty("in_act2_due_sched").Split('T')[0]);
        dtMaxDate = Convert.ToDateTime(strMaxDate);

        if(DateTime.Compare(dtDate,dtMaxDate)>0)
        {
            strMaxDate = dtDate.ToString("yyyy-MM-dd");
        }
    }
}

sql = "UPDATE [In_WorkOrder] SET [in_date_start]='"+strMinDate+"',[in_date_end]='"+strMaxDate+"' WHERE [id]='"+this.getID()+"'";
inn.applySQL(sql);

sql = "UPDATE [Workflow_Process] SET [active_date]='"+strMinDate+"',[closed_date]='"+strMaxDate+"'   WHERE [in_config_id]='"+this.getProperty("config_id")+"' and state='Active'";
inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CheckWorkOrderDetai' and [Method].is_current='1'">
<config_id>58FE258B998A482F8347F73DE2699477</config_id>
<name>In_CheckWorkOrderDetai</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
