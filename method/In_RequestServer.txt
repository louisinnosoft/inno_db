/*
目的:提供推播的資訊
做法:
1.提供notify-read的Queue
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string criteria = this.getProperty("criteria");
string UserInfo = this.getProperty("userinfo","");
string r = "";
 Innosoft.app _InnoApp = new Innosoft.app(inn,UserInfo);
 Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
 
string sql = "";
string strLoginName = _InnoApp.GetUserInfo(UserInfo, "loginid");
Item itmLoginIdentity = _InnH.GetIdentityByUserLoginName(strLoginName);

string aml = "";
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

string str_r = "";	
	
try
{
	//提供notify-read的Queue
	
	sql = "select * from [In_Queue] where in_type='notify-read' and state='Start' and owned_by_id='" +
	itmLoginIdentity.getID() + "'";
	
	aml = "<AML>";
	aml += "<Item type='In_Queue' action='get'>";
	aml += "<in_type>notify-read</in_type>";
	aml += "<state>Start</state>";
	aml += "<owned_by_id>" + itmLoginIdentity.getID()  + "</owned_by_id>";
	aml += "</Item></AML>";
	
	
	Item itmQueues = inn.applyAML(aml);
	for(int i=0;i<itmQueues.getItemCount();i++)
	{
		Item itmQueue = itmQueues.getItemByIndex(i);
		string strParam = itmQueue.getProperty("in_inparam","");
		strParam = "<param>" + strParam + "</param>";
		string strMessage = Innosoft.InnUtility.GetXmlNodeText(strParam,"message","");
		string strTitle = Innosoft.InnUtility.GetXmlNodeText(strParam,"title","");
		string strUrl = Innosoft.InnUtility.GetXmlNodeText(strParam,"url","");
		string strIcourl = Innosoft.InnUtility.GetXmlNodeText(strParam,"icourl","");
		
		str_r += "<notify_read>";
		str_r += "<queue_id>" + itmQueue.getID() + "</queue_id>";
		str_r += "<title>" + strTitle + "</title>";
		str_r += "<message>" + strMessage + "</message>";
		str_r += "<url><![CDATA[" + strUrl + "]]></url>";
		str_r += "<ico_url><![CDATA[" + strIcourl + "]]></ico_url>";
		str_r += "<type>notify-read</type>";
		str_r += "</notify_read>";
		
		//itmQueue.promote("In_Process","");
		
	}
	
	r = _InnoApp.BuildResponse("true",str_r,"",criteria);
	
}
catch (Exception ex)
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	criteria = "<request><![CDATA[" + criteria + "]]></request>";
	r = _InnoApp.BuildResponse("false",ex.Message,"",criteria);
}

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return  inn.newResult(r);	
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_RequestServer' and [Method].is_current='1'">
<config_id>F6F83BDE2E714E519566C279CC59614A</config_id>
<name>In_RequestServer</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
